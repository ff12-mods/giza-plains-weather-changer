// File size: 81392 Bytes
// Author:    mtanaka
// Source:    rbn_a18.src
// Date:      00/00 00:00
// Binary:    section_000.bin

option authorName = "mtanaka/ffgriever";
option fileName = "rbn_a18-weather.src";
option dataFile = "rbn_a18.src.data";
option spawnOrder = {-1, 1, -1, -1, -1, -1, -1, -1};
option positionFlags = 262151;
option unknownFlags1 = 65535;
option unknownScale = {100, 0, 0};
option unknownPosition2d = {0, 0};

//======================================================================
//                Global and scratchpad variable imports                
//======================================================================
import global   short   global_flag[256] = 0x0;
import global   short   シナリオフラグ = 0x0;
import global   u_char  g_btl_クリスタルミミックグリーン = 0xa31;
import global   u_char  g_btl_クリスタルミミックブルー = 0xa32;
import global   u_char  g_btl_クリスタルミミックレッド = 0xa33;
import global   u_char  quest_global_flag[94] = 0xb14;
import global   u_short g_menu_telepo_dest[31] = 0xbf2;
import global   float   g_com_navi_footcalc[3] = 0xc7c;
import global   int     g_com_set_get_label = 0xc88;
import global   int     g_com_set_get_label_2 = 0xc8c;
import global   int     g_com_set_get_label_3 = 0xcf0;
import global   u_char  mp_sav初回 = 0x984;
import global   u_char  mp_sav補正時間 = 0x985;
import global   u_short mp_sav初訪時間 = 0x986;
import global   u_char  mp_sav季節 = 0x988;
import global   float   mp_map_set_angle = 0x900;
import global   float   mp_map_set_x = 0x904;
import global   float   mp_map_set_y = 0x908;
import global   float   mp_map_set_z = 0x90c;
import global   int     mp_map_flg = 0x910;
import global   char    mp_last_weather = 0x940;
import global   char    mp_4map = 0x941;
import global   char    mp_map_set_hitse = 0x943;
import global   u_char  global_var_17 = 0x9aa;
import global   u_char  mp_giza_rains = 0x9ad;
import global   char    g_iw_mogxi_use = 0x402;
import global   u_char  g_iw_便利フラグ[1] = 0x405;
import global   char    g_iw_mogxi_rab_ok = 0x407;
import global   u_char  g_iw_ヴィエラ進行 = 0x40e;
import global   u_char  g_iw_mogxi_first_use = 0x411;
import global   u_char  g_iw_便利フラグ２[1] = 0x42b;
import global   u_char  g_iw_カトリーヌランダム出現場所フラグ = 0x431;
import global   u_char  global_var_20 = 0x482;
import scratch1 short   scratch1_var_2c = 0x12;
import scratch1 short   scratch1_var_2d = 0x20;
import scratch2 char    scratch2_var_2e = 0xc0;
import scratch2 u_char  scratch2_var_2f = 0xc1;



script 振動処理(0)
{

	function init()
	{
		return;
	}


	function 伐採振動開始()
	{
		wait(10);
		vibplay(3);
		vibsync();
		vibplay(6);
		vibsync();
		wait(10);
		vibplay(1);
		wait(10);
		vibstop();
		vibplay(8);
		vibsync();
		return;
	}


	function 大きな門振動開始()
	{
		vibplay(6);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(8);
		vibsync();
		return;
	}


	function ＤＴ扉振動開始()
	{
		vibplay(3);
		vibsync();
		vibplay(0);
		vibsync();
		return;
	}


	function ＤＴ門振動開始()
	{
		vibplay(6);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(3);
		wait(20);
		vibstop();
		vibplay(7);
		wait(25);
		vibstop();
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
int     file_var_24;             // pos: 0x34;


script telepoeffectctrl(0)
{

	function init()
	{
		return;
	}


	function telepoeffect()
	{
		sysSysreqwait(system_crystal_actor::touchon);
		pausesestop();
		subitem(0x2000, 1);
		sebsoundplay(0, 95);
		effectteleposyncbyid(partycommoneffectplay(12));
		whiteout_d1(2, 0);
		if ((getsummonmodebit() || getchocobomodebit()))
		{
			sysReq(1, choco_sum_ctrl_murasawa::sumcho_byebye);
			sysReqwait(choco_sum_ctrl_murasawa::sumcho_byebye);
		}
		switch (file_var_24)
		{
			case 0x8040:
				mapjump(0x132, 4, 10);
				break;
			case 0x8042:
				mapjump(0x276, 0, 10);
				break;
			case 0x8043:
				mapjump(66, 0, 10);
				break;
			case 0x8044:
				mapjump(157, 4, 10);
				break;
			case 0x8045:
				mapjump(176, 0, 10);
				break;
			case 0x8047:
				mapjump(0x2f4, 3, 10);
				break;
			case 0x8048:
				mapjump(188, 3, 10);
				break;
			case 0x8049:
				mapjump(0x2ce, 0, 10);
				break;
			case 0x804a:
				mapjump(0x317, 0, 10);
				break;
			case 0x804b:
				mapjump(0x168, 0, 10);
				break;
			case 0x804c:
				mapjump(0x193, 3, 10);
				break;
			case 0x804e:
				mapjump(0x1c2, 4, 10);
				break;
			case 0x8050:
				mapjump(0x2ae, 1, 10);
				break;
			case 0x8051:
				mapjump(0x252, 3, 10);
				break;
			case 0x8052:
				mapjump(0x337, 0, 10);
				break;
			case 0x8053:
				mapjump(0x44d, 0, 10);
				break;
			case 0x8054:
				mapjump(0x2ef, 0, 10);
				break;
			case 0x8055:
				mapjump(0x300, 4, 10);
				break;
			case 0x8056:
				mapjump(0x2e6, 3, 10);
				break;
			case 0x8057:
				mapjump(129, 0, 10);
				break;
			case 0x8058:
				mapjump(0x15e, 0, 10);
				break;
			case 0x8059:
				mapjump(0x47f, 7, 10);
				break;
			case 0x805a:
				mapjump(0x182, 1, 10);
				break;
			case 0x805b:
				mapjump(0x1e8, 0, 10);
				break;
			default:
				mapjump(142, 3, 10);
				break;
		}
		return;
	}
}


script イベント特殊効果画面(0)
{

	function init()
	{
		return;
	}


	function だいじなものゲット()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function システムメッセージ表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setmeswinmesid(0, g_com_set_get_label);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function だいじなものゲット_FADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function システムメッセージ表示_FADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setmeswinmesid(0, g_com_set_get_label);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function だいじなものゲット_MJFADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout_d0(2, 15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function ゲット＆メッセージ表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function ゲット＆ゲット()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label_2, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function GET_SYSMES_GET表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(6);
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label_3, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function GET_SYSMES_SYSMES表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_3);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}
}


script choco_sum_ctrl_murasawa(0)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function sumcho_byebye()
	{
		if (getsummonmodebit())
		{
			sethpmenufast(0);
			settimestopmagic();
			set_summon_effect_filter_status(0, 0);
			disposesummon();
			summontopartyread();
			summontopartyreadsync();
			changetonormalparty();
			termsummoncall();
			cleartimestopmagic();
		}
		if (getchocobomodebit())
		{
			setchocobomode(0);
			partyallread();
			releasemapjumpgroupflag(115);
		}
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_23;             // pos: 0x33;


script system_terepo_ctrl(0)
{

	function init()
	{
		file_var_23 = 0;
		if (((getmapjumpmode() & 8) && nowjumpindex() == gettelepoindex()))
		{
			sysReqiall(1, reqArr0);
			sysReqwaitall(reqArr0);
			setmapjumpmode((getmapjumpmode() | 16));
			if (file_var_23 == 0)
			{
				fadecancel_15a(2);
				whiteout(0);
				setforcefightmode(0);
				resetbehindcamera((getmapjumpanglebyindex(nowjumpindex()) + 3.14159274), 0);
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				sethpmenu(0);
			}
		}
		return;
	}


	function main(1)
	{
		if (((getmapjumpmode() & 8) && nowjumpindex() == gettelepoindex()))
		{
			if (file_var_23 == 0)
			{
				setforcefightmode(0);
				sebsoundplay(0, 96);
				calltelepoeffect();
				wait(3);
				fadein(0);
				wait(105);
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				sethpmenu(1);
			}
		}
		file_var_23 = 0;
		sysReqiall(1, reqArr1);
		sysReqwaitall(reqArr1);
		return;
	}
}


script saveactor(0)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_31;            // pos: 0x4;

	function init()
	{
		local_var_31 = 0;
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_30;            // pos: 0x0;

	function save_load()
	{
		local_var_30 = 0;
		debug_freeiopvoice();
		debug_reloadmc();
		debug_mc_set_port_slot(0, 0);
		regI0 = debug_mc_connect_check();
		if (regI0)
		{
			sysSystem(regI0);
			mesopenspeed(6, 0);
			ames(6, 0x100000f, 0x3c0, 0x21c, 4);
			messync(6, 1);
		}
		else
		{
			if (!(local_var_31))
			{
				mesopenspeed(6, 0);
				regI0 = aask(6, 0x1000010, 0x3c0, 0x21c, 4);
			}
			else
			{
				regI0 = 2;
			}
			regI1 = debug_mc_format_check();
			switch (regI0)
			{
				case 0:
					break;
				case 1:
					sysReqew(2, saveactor::load);
					break;
				case 2:
					sysReqew(2, saveactor::save);
					break;
			}
		}
		debug_recoveriopvoice();
		debug_unloadmc();
		if (local_var_30)
		{
			debug_load_mapjump();
			while (true)
			{
				wait(1);
			}
		}
		return;
	}


	function save()
	{
		if (regI1)
		{
			mesopenspeed(6, 0);
			regI0 = aask(6, 0x1000011, 0x3c0, 0x21c, 4);
			if (regI0 == 0)
			{
				return;
			}
			mesopenspeed(6, 0);
			ames(6, 0x1000012, 0x3c0, 0x21c, 4);
			debug_mc_format();
			mesclose(0);
		}
		if (!(local_var_31))
		{
			mesopenspeed(6, 0);
			regI0 = aask(6, 0x1000013, 0x3c0, 0x21c, 4);
		}
		else
		{
			regI0 = 1;
		}
		switch (regI0)
		{
			case 0:
				break;
			default:
				regI0 = (regI0 - 1);
				regI0 = debug_mc_write_savedata(regI0);
				mesopenspeed(6, 0);
				if (regI0)
				{
					ames(6, 0x1000014, 0x3c0, 0x21c, 4);
				}
				else
				{
					ames(6, 0x1000015, 0x3c0, 0x21c, 4);
				}
				if (!(local_var_31))
				{
					messync(6, 1);
				}
				else
				{
					messync2(6);
					mesclose(6);
				}
				break;
		}
		return;
	}


	function load()
	{
		if (regI1)
		{
			sysSystem(regI1);
			mesopenspeed(6, 0);
			ames(6, 0x1000016, 0x3c0, 0x21c, 4);
			messync(6, 1);
			return;
		}
		mesopenspeed(6, 0);
		regI0 = aask(6, 0x1000017, 0x3c0, 0x21c, 4);
		switch (regI0)
		{
			case 0:
				break;
			default:
				regI0 = (regI0 - 1);
				regI0 = debug_mc_read_savedata(regI0);
				if (regI0)
				{
					mesopenspeed(6, 0);
					ames(6, 0x1000018, 0x3c0, 0x21c, 4);
					messync(6, 1);
				}
				else
				{
					local_var_30 = 1;
				}
				break;
		}
		return;
	}
}


script __MJ_CTRL000(0)
{

	function init()
	{
		reqenable(12);
		setmapjumpgroup(1);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_32;            // pos: 0x0;

	function mapjump(12)
	{
		local_var_32 = 0;
		#include "include/calculation.c"
		if (mp_sav季節 == 1)
		{
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				spotsoundtrans(24, 0);
				goto localjmp_10;
				goto localjmp_11;
			localjmp_10:
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 12);
			localjmp_11:
				getmapdestposbyindex(3, &mp_map_set_x, &mp_map_set_y, &mp_map_set_z, &mp_map_set_angle);
				for (regI0 = 1; regI0 < 15; regI0 = (regI0 + 1))
				{
					setmapidmj(regI0, 1, 1);
				}
				if ((check_move_battlestatus(0) == 1 || istownmap() == 1))
				{
					ucmove_52b(0, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				if ((check_move_battlestatus(1) == 1 || istownmap() == 1))
				{
					ucmove_52b(1, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				if ((check_move_battlestatus(2) == 1 || istownmap() == 1))
				{
					ucmove_52b(2, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				if ((check_move_battlestatus(3) == 1 || istownmap() == 1))
				{
					ucmove_52b(3, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				wait(14);
				stopspotsound();
				pausesestop();
				hideparty();
				voicestopall();
				setbattlethinkstatus_freetarget_group(-1, 0);
				mapjump(236, 1, 0);
			}
		}
		else
		{
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				spotsoundtrans(24, 0);
				goto localjmp_26;
				goto localjmp_27;
			localjmp_26:
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 12);
			localjmp_27:
				getmapdestposbyindex(3, &mp_map_set_x, &mp_map_set_y, &mp_map_set_z, &mp_map_set_angle);
				for (regI0 = 1; regI0 < 15; regI0 = (regI0 + 1))
				{
					setmapidmj(regI0, 1, 1);
				}
				if ((check_move_battlestatus(0) == 1 || istownmap() == 1))
				{
					ucmove_52b(0, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				if ((check_move_battlestatus(1) == 1 || istownmap() == 1))
				{
					ucmove_52b(1, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				if ((check_move_battlestatus(2) == 1 || istownmap() == 1))
				{
					ucmove_52b(2, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				if ((check_move_battlestatus(3) == 1 || istownmap() == 1))
				{
					ucmove_52b(3, mp_map_set_x, mp_map_set_y, mp_map_set_z);
				}
				wait(14);
				stopspotsound();
				pausesestop();
				hideparty();
				voicestopall();
				setbattlethinkstatus_freetarget_group(-1, 0);
				mapjump(253, 1, 0);
			}
		}
		if (local_var_32)
		{
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				spotsoundtrans(40, 0);
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 12);
				for (regI0 = 1; regI0 <= 15; regI0 = (regI0 + 1))
				{
					setmapidmj(regI0, 1, 1);
				}
				if ((check_move_battlestatus(0) == 1 || istownmap() == 1))
				{
					ucmove_52b(0, 124, -10, 365.025299);
				}
				if ((check_move_battlestatus(1) == 1 || istownmap() == 1))
				{
					ucmove_52b(1, 124, -10, 365.025299);
				}
				if ((check_move_battlestatus(2) == 1 || istownmap() == 1))
				{
					ucmove_52b(2, 124, -10, 365.025299);
				}
				if ((check_move_battlestatus(3) == 1 || istownmap() == 1))
				{
					ucmove_52b(3, 124, -10, 365.025299);
				}
				wait(12);
				stopspotsound();
				pausesestop();
				fadesync();
				wait(2);
				mapjump(236, 1, 0);
			}
		}
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_22;             // pos: 0x32;


script setup(0)
{

	function init()
	{
		return;
	}


	function event()
	{
		switch (シナリオフラグ)
		{
			case lt(0x157):
				break;
			case lte(0x157):
				file_var_22 = 1;
				stopenvsoundall();
				setcharseplayall(0);
				file_var_22 = 1;
				break;
			default:
				break;
		}
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_2b;             // pos: 0x60;


script 常駐監督(0)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_33;            // pos: 0x0;
	u_char  local_var_34;            // pos: 0x1;
	u_char  local_var_35;            // pos: 0x2;

	function init()
	{
		switch (シナリオフラグ)
		{
			case lt(0x157):
				break;
			case lte(0x157):
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				if (nowjumpindex() == gettelepoindex())
				{
					whiteout(0);
					fadecancel_15a(2);
				}
				else
				{
					fadeout(1);
					fadecancel_15a(2);
				}
				setcharseplayall(0);
				local_var_33 = 1;
				eventread(108);
				eventreadsync();
				break;
			default:
				break;
		}
		modelread(0x300000e);
		if (シナリオフラグ >= 0x155)
		{
			g_iw_mogxi_rab_ok = 1;
			motionread(2);
			motionreadsync(2);
			sysReqi(1, モグシー::MogxiSet);
			sysReqwait(モグシー::MogxiSet);
			sysReq(1, モグシー::MogxiGrad);
		}
		modelreadsync(0x300000e);
		if (g_iw_mogxi_use != 0)
		{
			setposparty(120.632599, -10, 157.770218, 1.662714);
			g_iw_mogxi_use = 0;
			scratch2_var_2f = g_iw_カトリーヌランダム出現場所フラグ;
			resetbehindcamera(-1.47887874, 0.5);
			if (local_var_34 == 0)
			{
				setmapjumpmode((getmapjumpmode() | 16));
				fadecancel_15a(2);
				whiteout(0);
				setforcefightmode(0);
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				suspendbattle();
				sethpmenu(0);
				local_var_35 = 1;
			}
			else
			{
				setmapjumpmode((getmapjumpmode() | 16));
				setforcefightmode(0);
				local_var_35 = 0;
			}
		}
		else
		{
			local_var_35 = 0;
		}
		return;
	}


	function main(1)
	{
		if (local_var_35 != 0)
		{
			setforcefightmode(0);
			sebsoundplay(0, 96);
			calltelepoeffect();
			wait(3);
			fadein(0);
			wait(105);
			resumebattle();
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			sethpmenu(1);
		}
		regY = シナリオフラグ;
		goto localjmp_2;
		while (true)
		{
			sysReqi(1, 配置監督::ベース配置);
			sysReqwait(配置監督::ベース配置);
			break;
		localjmp_2:
		}
		if (((シナリオフラグ >= 0x668 && g_iw_ヴィエラ進行 >= 0) && g_iw_ヴィエラ進行 < 1))
		{
			sysReqi(1, 放浪ヴィエラ::vie_set);
			sysReqwait(放浪ヴィエラ::vie_set);
			sysReq(1, 放浪ヴィエラ::vie_action);
		}
		switch (シナリオフラグ)
		{
			case lt(0x157):
				break;
			case between(0x157, 0x33e):
				motionread(1);
				motionreadsync(1);
				sysReqiall(1, reqArr2);
				sysReqwaitall(reqArr2);
				sysReqall(1, reqArr3);
				break;
			default:
				break;
		}
		switch (シナリオフラグ)
		{
			case lt(0x157):
				break;
			case lte(0x157):
				sysReqall(2, reqArr4);
				waitv(&file_var_2b);
				sethpmenu(0);
				sysReq(1, system_crystal_actor::showoff);
				sysReqall(2, reqArr5);
				sysReqall(3, reqArr6);
				eventplay();
				eventsync();
				シナリオフラグ = 0x15e;
				healall(2);
				wait(1);
				removeguestbattlemember(9);
				removepartymember(3);
				removepartymember(2);
				showparty();
				wait(1);
				setbattlemember(0, -1, -1);
				showparty();
				wait(1);
				sysReqall(4, reqArr7);
				sysReq(1, system_crystal_actor::showon);
				sysReqall(2, reqArr8);
				startenvsoundall();
				setcharseplayall(1);
				setstatuserrordispdenystatus(0);
				clearnavimapfootmark();
				sethpmenufast(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				wait(1);
				setcharseplayall(1);
				fadein(15);
				sethpmenu(1);
				sysReqall(3, reqArr9);
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				break;
			default:
				break;
		}
		if (nowjumpindex() == 3)
		{
			setmapjumpgroupflag(28);
		}
		return;
	}
}


script 配置監督(0)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function ベース配置()
	{
		sysReq(1, hitactor01::setHitObj);
		sysReq(1, hitactor02::setHitObj);
		sysReq(1, hitactor03::setHitObj);
		sysReq(1, hitactor04::setHitObj);
		sysReq(1, hitactor05::setHitObj);
		sysReq(1, hitactor06::setHitObj);
		motionread(0);
		motionreadsync(0);
		sysReqchg(2, NPC04::talk_step01);
		sysReqchg(2, NPC05::talk_step01);
		sysReqchg(2, NPC06::talk_step01);
		sysReqchg(2, NPC07::talk_step01);
		sysReqchg(2, NPC08::talk_step01);
		sysReqchg(2, NPC17::talk_crystal);
		sysReqchg(2, NPC01::talk_gardy);
		sysReqchg(16, NPC01::talkhold_gardy);
		sysReqchg(17, NPC01::talkterm_gardy);
		sysReqiall(2, reqArr10);
		if ((シナリオフラグ >= 0x604 && シナリオフラグ < 0x668))
		{
			sysReqiall(1, reqArr11);
			sysReqwaitall(reqArr11);
			sysReqall(1, reqArr12);
		}
		return;
	}
}


script テレポ監督(0)
{

	function init()
	{
		return;
	}


	function checkTelepoCondition()
	{
		switch (シナリオフラグ)
		{
			case lt(0x157):
				file_var_23 = 0;
				break;
			case lte(0x157):
				file_var_23 = 1;
				break;
			default:
				file_var_23 = 0;
				break;
		}
		return;
	}


	function getTelepoFinished()
	{
		file_var_2b = 1;
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
int     file_var_28;             // pos: 0x50;
int     file_var_29;             // pos: 0x58;
int     file_var_2a;             // pos: 0x5c;


script quest_director(0)
{

	function init()
	{
		return;
	}


	function クエスト受諾メニュー読み込み()
	{
		if ((file_var_28 >= 0 && file_var_28 <= 49))
		{
		}
		return;
	}


	function クエスト受諾メニュー再生()
	{
		setquestorder(file_var_28, 1);
		setquestok(file_var_28, 0);
		return;
	}


	function メインクエスト受諾メニュー再生()
	{
		setquestorder(file_var_28, 1);
		setquestok(file_var_28, 0);
		return;
	}


	function メインクエスト受諾メニュー再生システムメッセージ付き()
	{
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setquestorder(file_var_28, 1);
		setquestok(file_var_28, 0);
		setmeswinmesid(0, file_var_29);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		return;
	}


	function メインクエスト受諾メニュー再生システムメッセージフェード付き()
	{
		musictrans(-1, 30, 32);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setquestorder(file_var_28, 1);
		setquestok(file_var_28, 0);
		setmeswinmesid(0, file_var_29);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		return;
	}


	function メインクエスト受諾メニュー再生システムメッセージx2付き()
	{
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setquestorder(file_var_28, 1);
		setquestok(file_var_28, 0);
		setmeswinmesid(0, file_var_29);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		setmeswinmesid(0, file_var_2a);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		return;
	}


	function メインクエスト受諾メニュー再生システムメッセージx2フェード付き()
	{
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setquestorder(file_var_28, 1);
		setquestok(file_var_28, 0);
		setmeswinmesid(0, file_var_29);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		setmeswinmesid(0, file_var_2a);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		return;
	}


	function クエストクリアメニュー読み込み()
	{
		return;
	}


	function クエストクリアメニュー再生()
	{
		setquestorder(file_var_28, 0);
		setquestclear(file_var_28, 1);
		setquestok(file_var_28, 0);
		return;
	}


	function クエストクリアメニュー再生フェード付き()
	{
		setquestorder(file_var_28, 0);
		setquestclear(file_var_28, 1);
		setquestok(file_var_28, 0);
		fadeout(15);
		return;
	}


	function クエストクリアメニュー再生システムメッセージ付き()
	{
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setquestorder(file_var_28, 0);
		setquestclear(file_var_28, 1);
		setquestok(file_var_28, 0);
		setmeswinmesid(0, file_var_29);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		return;
	}


	function クエストクリアメニュー再生フェード付きシステムメッセージ付き()
	{
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setquestorder(file_var_28, 0);
		setquestclear(file_var_28, 1);
		setquestok(file_var_28, 0);
		setmeswinmesid(0, file_var_29);
		ames_1fe(0, 0x100000e, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		return;
	}


	function 音楽復帰()
	{
		return;
	}


	function 終了音楽復帰()
	{
		return;
	}
}


script choco_sum_ctrl(0)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function summon_off_tag()
	{
		sethpmenufast(0);
		settimestopmagic();
		set_summon_effect_filter_status(0, 0);
		disposesummon();
		summontopartyread();
		summontopartyreadsync();
		sethpmenu(1);
		changetonormalparty();
		termsummoncall();
		cleartimestopmagic();
		return;
	}


	function sumcho_byebye()
	{
		if (geteventpartymodebit())
		{
			sethpmenufast(0);
			seteventpartymode(0);
			partyallread();
		}
		if (geteventparty2modebit())
		{
			sethpmenufast(0);
			seteventparty2mode(0);
			partyallread();
		}
		if (getsummonmodebit())
		{
			sethpmenufast(0);
			settimestopmagic();
			set_summon_effect_filter_status(0, 0);
			disposesummon();
			summontopartyread();
			summontopartyreadsync();
			sethpmenu(1);
			changetonormalparty();
			termsummoncall();
			cleartimestopmagic();
		}
		if (getchocobomodebit())
		{
			setchocobomode(0);
			partyallread();
			releasemapjumpgroupflag(115);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_36;            // pos: 0x0;

	function sumcho_byebye_fadeout()
	{
		local_var_36 = 0;
		if (geteventpartymodebit())
		{
			sethpmenufast(0);
			seteventpartymode(0);
			partyallread();
		}
		if (geteventparty2modebit())
		{
			sethpmenufast(0);
			seteventparty2mode(0);
			partyallread();
		}
		if ((getsummonmodebit() || getchocobomodebit()))
		{
			fadeout_d0(1, 15);
			fadesync_d3(1);
			if (getsummonmodebit())
			{
				sethpmenufast(0);
				settimestopmagic();
				set_summon_effect_filter_status(0, 0);
				disposesummon();
				summontopartyread();
				summontopartyreadsync();
				sethpmenu(1);
				changetonormalparty();
				termsummoncall();
				cleartimestopmagic();
			}
			if (getchocobomodebit())
			{
				setchocobomode(0);
				partyallread();
				releasemapjumpgroupflag(115);
			}
			local_var_36 = 1;
		}
		return;
	}


	function sumcho_byebye_fadein()
	{
		if (local_var_36 != 0)
		{
			fadein_d2(1, 15);
		}
		return;
	}
}


script mogxiMapJumpDirector(0)
{

	function init()
	{
		return;
	}


	function mogjump()
	{
		sysSysreqwait(モグシー::talk);
		if (scratch2_var_2e <= 4)
		{
			モグシー.motionplay(0x10000009);
			sebsoundplay(0, 95);
			effectteleposyncbyid(partycommoneffectplay(12));
			whiteout_d1(2, 0);
		}
		switch (scratch2_var_2e)
		{
			case 0:
				mapjump(0x123, 0, 10);
				break;
			case 1:
				mapjump(0x122, 0, 10);
				break;
			case 2:
				mapjump(0x131, 0, 10);
				break;
			case 3:
				mapjump(0x132, 0, 10);
				break;
			case 4:
				mapjump(0x133, 0, 10);
				break;
			default:
				g_iw_mogxi_use = 0;
				break;
		}
		return;
	}
}


script Map_Director(0)
{

	function init()
	{
		#include "include/calculation.c"
		if (mp_sav季節 == 1)
		{
			showmapmodel(5);
			hidemapmodel(6);
			unkCall_5c3(0);
		}
		else
		{
			hidemapmodel(5);
			showmapmodel(6);
			unkCall_5c3(1);
		}
		animeplay(2);
		switch (nowjumpindex())
		{
			case 1:
			case 3:
				resetbehindcamera(getmapjumpanglebyindex(nowjumpindex()), -0.400000006);
				break;
		}
		return;
	}


	function main(1)
	{
		switch (nowjumpindex())
		{
			case 1:
			case 3:
				resetbehindcamera(getmapjumpanglebyindex(nowjumpindex()), -0.400000006);
				break;
		}
		return;
	}
}


script gate_south(1)
{

	function init()
	{
		fieldsign(0);
		reqenable(2);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_37;            // pos: 0x0;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		mp_map_set_angle = 3.14159274;
		local_var_37 = 1;
		if (local_var_37 == 1)
		{
			pausesestop();
			sysReqew(1, map_ダミーＰＣ::扉オープン１);
			sysReqsw(1, 振動処理::大きな門振動開始);
			mp_map_flg = 0;
			startscene(6);
			mp_map_flg = mapsoundplay(6);
			animeplay(0);
			wait(60);
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				steppoint(3);
				mp_4map = 30;
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, mp_4map);
				steppoint(4);
				wait(15);
				steppoint(3);
				spotsoundtrans(15, 0);
				steppoint(2);
				wait(17);
				stopspotsound();
				pausesestop();
				hideparty();
				eventsoundplaysync(mp_map_flg);
				steppoint(1);
				setbattlethinkstatus_freetarget_group(-1, 0);
				voicestopall();
				mapjump(0x124, 6, 0);
			}
		}
		else
		{
			setnoupdatebehindcamera(0);
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_38;            // pos: 0x2;

	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		local_var_38 = 0;
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqenable(2);
		local_var_38 = 1;
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}
}


script mapダウンタウン扉(1)
{

	function init()
	{
		fieldsign(1);
		reqenable(2);
		if (シナリオフラグ >= 100)
		{
			fieldsignicon(2);
		}
		else
		{
			fieldsignicon(3);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_39;            // pos: 0x0;
	char    local_var_3a;            // pos: 0x2;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		mp_map_set_angle = 1.57079637;
		if (local_var_3a)
		{
			sysReqew(1, map_ダミーＰＣ::ターン);
			setmeswincaptionid(1, 3);
			amese(1, 0x1000047);
			messync(1, 1);
			setnoupdatebehindcamera(0);
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		else if (シナリオフラグ >= 100)
		{
			local_var_39 = 1;
			if (local_var_39 == 1)
			{
				pausesestop();
				sysReqew(1, map_ダミーＰＣ::扉オープン１);
				sysReqsw(1, 振動処理::ＤＴ門振動開始);
				mp_map_flg = 0;
				startscene(9);
				mp_map_flg = mapsoundplay(9);
				animeplay(3);
				wait(90);
				regI3 = sysucoff();
				if (regI3)
				{
					clearmapjumpstatus();
					sysucon();
				}
				else
				{
					mp_last_weather = getweatherslot();
					steppoint(3);
					mp_4map = 30;
					fadelayer(6);
					fadeprior(255);
					fadeout_d0(2, mp_4map);
					steppoint(4);
					wait(15);
					steppoint(3);
					spotsoundtrans(15, 0);
					steppoint(2);
					wait(17);
					stopspotsound();
					pausesestop();
					hideparty();
					eventsoundplaysync(mp_map_flg);
					steppoint(1);
					setbattlethinkstatus_freetarget_group(-1, 0);
					voicestopall();
					mapjump(0x2be, 4, 0);
				}
			}
			else
			{
				setnoupdatebehindcamera(0);
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
		}
		else
		{
			sysReqew(1, map_ダミーＰＣ::ターン);
			setmeswincaptionid(1, 3);
			amese(1, 0x1000047);
			messync(1, 1);
			setnoupdatebehindcamera(0);
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		return;
	}


	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		local_var_3a = 0;
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqenable(2);
		local_var_3a = 1;
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
int     file_var_25;             // pos: 0x38;
int     file_var_26;             // pos: 0x3c;


script system_crystal_actor(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_3b;            // pos: 0x0;
	int     local_var_3c;            // pos: 0x4;
	int     local_var_41;            // pos: 0x1c;

	function init()
	{
		enable_by_summon(1);
		file_var_26 = 0;
		if (haveitem(0x8040) == 0)
		{
			additem_47f(0x8040, 1, 0);
			for (regI0 = 0; regI0 < 31; regI0 = (regI0 + 1))
			{
				if (g_menu_telepo_dest[regI0] == 0x8040)
				{
					break;
				}
			}
			if (regI0 == 31)
			{
				for (regI0 = 0; regI0 < 31; regI0 = (regI0 + 1))
				{
					if (g_menu_telepo_dest[regI0] == 0)
					{
						g_menu_telepo_dest[regI0] = 0x8040;
						break;
					}
				}
			}
		}
		hide();
		if (nowjumpindex() == getsaveindex())
		{
			resetbehindcamera((getmapjumpanglebyindex(nowjumpindex()) + 3.14159274), 0);
		}
		local_var_3b = getcrystalid(3, 0);
		if (local_var_3b == 4)
		{
			if (ismapjumpgroupflag(0x402a) != 1)
			{
				local_var_3b = 0;
			}
		}
		if (!((local_var_3b < 0 || unkCall_5b4())))
		{
			setposbynaviicon(3, 0);
			local_var_41 = 1;
			switch (nowmap())
			{
				case 176:
					if (g_btl_クリスタルミミックレッド == 2)
					{
						local_var_41 = 0;
					}
					break;
				case 186:
					if (g_btl_クリスタルミミックブルー == 2)
					{
						local_var_41 = 0;
					}
					break;
				case 0x260:
					if (g_btl_クリスタルミミックグリーン == 2)
					{
						local_var_41 = 0;
						setmapjumpgroupflag(127);
						releasemapjumpgroupflag(128);
					}
					else
					{
						setmapjumpgroupflag(128);
						releasemapjumpgroupflag(127);
					}
					break;
				default:
					local_var_41 = 0;
					break;
			}
			switch (local_var_3b)
			{
				case 0:
					if (getsaveramsavestatus())
					{
						if (nowmap() == 0x193)
						{
							bindp2(0x3000000);
						}
						else
						{
							bindp2(0x3000001);
						}
						reqenable(4);
						reqenable(5);
						reqdisable(3);
						reqenable(8);
						reqenable(13);
						setfreetargettype(4);
						fieldsignicon(2);
					}
					break;
				case 255:
					if (nowmap() == 0x193)
					{
						bindp2(0x3000000);
					}
					else
					{
						bindp2(0x3000002);
					}
					reqenable(4);
					reqenable(5);
					reqdisable(3);
					reqenable(8);
					reqenable(13);
					setfreetargettype(4);
					fieldsignicon(2);
					break;
				default:
					if (nowmap() == 0x193)
					{
						bindp2(0x3000000);
					}
					else
					{
						bindp2(0x3000003);
					}
					reqenable(4);
					reqenable(5);
					reqdisable(3);
					reqenable(8);
					reqenable(13);
					setfreetargettype(4);
					fieldsignicon(2);
					break;
			}
		}
		if (!(unkCall_5b4()))
		{
			set_ignore_hitgroup(255);
			setweight(-1);
			touchradius(2);
			talkang(6.28318548);
			local_var_3c = 0;
			settalkiconstatus(0);
			settouchuconly(1);
		}
		if (!((local_var_3b < 0 || unkCall_5b4())))
		{
			switch (local_var_3b)
			{
				case 0:
					setnpcname(0x1d5);
					break;
				case 255:
					setnpcname(0x1d3);
					break;
				case 1:
					setnpcname(0x1b3);
					break;
				case 3:
					setnpcname(0x1b4);
					break;
				case 4:
					setnpcname(0x1b5);
					break;
				case 5:
					setnpcname(0x1bd);
					break;
				case 6:
					setnpcname(0x1be);
					break;
				case 8:
					setnpcname(0x1c9);
					break;
				case 9:
					setnpcname(0x1c8);
					break;
				case 10:
					setnpcname(0x1c0);
					break;
				case 11:
					setnpcname(0x1b8);
					break;
				case 12:
					setnpcname(0x1b9);
					break;
				case 13:
					setnpcname(0x1bb);
					break;
				case 15:
					setnpcname(0x1c1);
					break;
				case 17:
					setnpcname(0x1c3);
					break;
				case 18:
					setnpcname(0x1c5);
					break;
				case 19:
					setnpcname(0x1ca);
					break;
				case 20:
					setnpcname(0x1cb);
					break;
				case 21:
					setnpcname(0x1bf);
					break;
				case 22:
					setnpcname(0x1c2);
					break;
				case 23:
					setnpcname(0x1c4);
					break;
				case 24:
					setnpcname(0x1b6);
					break;
				case 25:
					setnpcname(0x1b7);
					break;
				case 28:
					setnpcname(0x1bc);
					break;
				case 29:
					setnpcname(0x1ba);
					break;
				case 32:
					setnpcname(0x1c6);
					break;
				case 33:
					setnpcname(0x1c7);
					break;
				default:
					setnpcname(0x1d2);
					break;
			}
		}
		return;
	}


	function main(1)
	{
		if (file_var_22 == 0)
		{
			if (local_var_41 == 0)
			{
				show_38(1);
				usecharhit(1);
				reqdisable(3);
				reqenable(8);
				reqenable(13);
				reqenable(2);
			}
			else
			{
				reqdisable(2);
				reqdisable(8);
				reqdisable(13);
				usecharhit(0);
				show_38(0);
			}
		}
		else
		{
			reqdisable(2);
			reqdisable(8);
			reqdisable(13);
			usecharhit(0);
			show_38(0);
			file_var_22 = 0;
		}
		while (true)
		{
			if (local_var_3c)
			{
				charefftrigger(2);
				local_var_3c = 0;
			}
			wait(1);
		}
	}


	function touchon(4)
	{
		if (local_var_3c == 0)
		{
			charefftrigger(1);
			local_var_3c = 1;
		}
		return;
	}

	symlink touch(touchon) : 3;

	symlink fieldsign1(touchon) : 8;

	symlink fieldsign2(touchon) : 13;


	function touchoff(5)
	{
		if (local_var_3c)
		{
			charefftrigger(2);
			local_var_3c = 0;
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_3d;            // pos: 0x8;
	int     local_var_3e;            // pos: 0xc;
	int     local_var_3f;            // pos: 0x14;
	int     local_var_40;            // pos: 0x18;

	function talk(2)
	{
		file_var_26 = 1;
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		suspendbattle();
		sethpmenu(0);
		file_var_24 = 0;
		wait(7);
		healall(0);
		btladjustsystem(1);
		if (sysIsdead2_49f(PC00) != 0)
		{
			sysCommon2effectplay(PC00, 11);
		}
		if (sysIsdead2_49f(PC01) != 0)
		{
			sysCommon2effectplay(PC01, 11);
		}
		if (sysIsdead2_49f(PC02) != 0)
		{
			sysCommon2effectplay(PC02, 11);
		}
		if (sysIsdead2_49f(PC03) != 0)
		{
			sysCommon2effectplay(PC03, 11);
		}
		regI0 = sebsoundplay(0, 45);
		wait(1);
		sysReqew(1, crystaldummyPC::クリスタルビュー);
		wait(5);
		if ((local_var_3b > 0 && local_var_3b != 255))
		{
			sysDVAR(0, local_var_3e);
			local_var_3f = 0;
			switch (local_var_3b)
			{
				case 1:
					local_var_3f = 0x8040;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 3:
					local_var_3f = 0x8042;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 4:
					local_var_3f = 0x8043;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 5:
					local_var_3f = 0x8044;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 6:
					local_var_3f = 0x8045;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 8:
					local_var_3f = 0x8047;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 9:
					local_var_3f = 0x8048;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 10:
					local_var_3f = 0x8049;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 11:
					local_var_3f = 0x804a;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 12:
					local_var_3f = 0x804b;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 13:
					local_var_3f = 0x804c;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 15:
					local_var_3f = 0x804e;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 17:
					local_var_3f = 0x8050;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 18:
					local_var_3f = 0x8051;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 19:
					local_var_3f = 0x8052;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 20:
					local_var_3f = 0x8053;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 21:
					local_var_3f = 0x8054;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 22:
					local_var_3f = 0x8055;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 23:
					local_var_3f = 0x8056;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 24:
					local_var_3f = 0x8057;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 25:
					local_var_3f = 0x8058;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 28:
					local_var_3f = 0x8059;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 29:
					local_var_3f = 0x805a;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 32:
					local_var_3f = 0x805b;
					local_var_3e = haveitem(local_var_3f);
					break;
				case 33:
					local_var_3f = 0x805c;
					local_var_3e = haveitem(local_var_3f);
					break;
				default:
					debug_tty_string(0x3000000);
					local_var_3e = -1;
					break;
			}
			sysDVAR(0, local_var_3e);
			sysDVAR(1, local_var_3b);
			sysDVAR(3, local_var_3f);
			if (local_var_3e == 0)
			{
				switch (local_var_3b)
				{
					case 1:
						additem_47f(0x8040, 1, 0);
						break;
					case 3:
						additem_47f(0x8042, 1, 0);
						break;
					case 4:
						additem_47f(0x8043, 1, 0);
						break;
					case 5:
						additem_47f(0x8044, 1, 0);
						break;
					case 6:
						additem_47f(0x8045, 1, 0);
						break;
					case 8:
						additem_47f(0x8047, 1, 0);
						break;
					case 9:
						additem_47f(0x8048, 1, 0);
						break;
					case 10:
						additem_47f(0x8049, 1, 0);
						break;
					case 11:
						additem_47f(0x804a, 1, 0);
						break;
					case 12:
						additem_47f(0x804b, 1, 0);
						break;
					case 13:
						additem_47f(0x804c, 1, 0);
						break;
					case 15:
						additem_47f(0x804e, 1, 0);
						break;
					case 17:
						additem_47f(0x8050, 1, 0);
						break;
					case 18:
						additem_47f(0x8051, 1, 0);
						break;
					case 19:
						additem_47f(0x8052, 1, 0);
						break;
					case 20:
						additem_47f(0x8053, 1, 0);
						break;
					case 21:
						additem_47f(0x8054, 1, 0);
						break;
					case 22:
						additem_47f(0x8055, 1, 0);
						break;
					case 23:
						additem_47f(0x8056, 1, 0);
						break;
					case 24:
						additem_47f(0x8057, 1, 0);
						break;
					case 25:
						additem_47f(0x8058, 1, 0);
						break;
					case 28:
						additem_47f(0x8059, 1, 0);
						break;
					case 29:
						additem_47f(0x805a, 1, 0);
						break;
					case 32:
						additem_47f(0x805b, 1, 0);
						break;
					case 33:
						additem_47f(0x805c, 1, 0);
						break;
					default:
						debug_tty_string(0x300001f);
						break;
				}
				for (regI0 = 0; regI0 < 31; regI0 = (regI0 + 1))
				{
					if (g_menu_telepo_dest[regI0] == local_var_3f)
					{
						break;
					}
				}
				if (regI0 == 31)
				{
					for (regI0 = 0; regI0 < 31; regI0 = (regI0 + 1))
					{
						if (g_menu_telepo_dest[regI0] == 0)
						{
							g_menu_telepo_dest[regI0] = local_var_3f;
							break;
						}
					}
				}
			}
			setmesmacro(0, 0, 0, haveitem(0x2000));
			if (getsaveramsavestatus())
			{
				if (getsaveramtelepostatus())
				{
					fs_stonewinopen(haveitem(0x2000));
					setmeswincaptionid(0, 2);
					askpos(0, 0, 2);
					local_var_3d = aaske(0, 0x1000001);
					mesclose(0);
					fs_stonewinclose();
					messync(0, 1);
					switch (local_var_3d)
					{
						case 0:
							if (scratch1_var_2c)
							{
								if (nowmap() == 0x309)
								{
									setmeswincaptionid(0, 3);
									ames(0, 0x1000002, 0x3c0, 0x21c, 4);
								}
								else
								{
									setmeswincaptionid(0, 2);
									amese(0, 0x1000003);
								}
								messync(0, 1);
								mesclose(0);
							}
							messync(0, 1);
							local_var_40 = nowjumpindex();
							if ((nowmap() == 0x193 && global_flag[0] < 0x541))
							{
								healall(0);
							}
							else
							{
								setnowjumpindex(-2);
							}
							fadelayer(5);
							fadeout(10);
							wait(10);
							openfullscreenmenu_48d(0x8005, 0);
							fadein(10);
							wait(10);
							setnowjumpindex(local_var_40);
							break;
						case 1:
							if (haveitem(0x2000) == 0)
							{
								setmeswincaptionid(0, 2);
								setmesmacro(0, 0, 1, 0x2000);
								amese(0, 0x1000004);
								messync(0, 1);
								mesclose(0);
							}
							else
							{
								fs_stonewinopen(haveitem(0x2000));
								regI2 = 0;
								for (regI0 = 0; regI0 < 25; regI0 = (regI0 + 1))
								{
									if (g_menu_telepo_dest[regI0] != 0)
									{
										setmesmacro(0, regI0, 1, g_menu_telepo_dest[regI0]);
										regI2 = (regI2 + 1);
									}
									else
									{
										setaskselectignore(0, regI0);
									}
								}
								if (regI2 > 6)
								{
									regI3 = 7;
								}
								else
								{
									regI3 = (regI2 + 1);
								}
								setmeswinline(0, regI3);
								askpos(0, 0, regI2);
								local_var_3d = aask(0, 0x1000005, 120, 0x3fe, 1);
								fs_stonewinclose();
								if (local_var_3d >= 25)
								{
									regI1 = 0;
								}
								else
								{
									regI1 = g_menu_telepo_dest[local_var_3d];
									switch (regI1)
									{
										case 0x8040:
											if (nowmap() == 0x132)
											{
												regI1 = -1;
											}
											break;
										case 0x8042:
											if (nowmap() == 0x276)
											{
												regI1 = -1;
											}
											break;
										case 0x8043:
											if (nowmap() == 66)
											{
												regI1 = -1;
											}
											break;
										case 0x8044:
											if (nowmap() == 157)
											{
												regI1 = -1;
											}
											break;
										case 0x8045:
											if (nowmap() == 176)
											{
												regI1 = -1;
											}
											break;
										case 0x8047:
											if (nowmap() == 0x2f4)
											{
												regI1 = -1;
											}
											break;
										case 0x8048:
											if (nowmap() == 188)
											{
												regI1 = -1;
											}
											break;
										case 0x8049:
											if (nowmap() == 0x2ce)
											{
												regI1 = -1;
											}
											break;
										case 0x804a:
											if (nowmap() == 0x317)
											{
												regI1 = -1;
											}
											break;
										case 0x804b:
											if (nowmap() == 0x168)
											{
												regI1 = -1;
											}
											break;
										case 0x804c:
											if (nowmap() == 0x193)
											{
												regI1 = -1;
											}
											break;
										case 0x804e:
											if (nowmap() == 0x1c2)
											{
												regI1 = -1;
											}
											break;
										case 0x8050:
											if (nowmap() == 0x2ae)
											{
												regI1 = -1;
											}
											break;
										case 0x8051:
											if (nowmap() == 0x252)
											{
												regI1 = -1;
											}
											break;
										case 0x8052:
											if (nowmap() == 0x337)
											{
												regI1 = -1;
											}
											break;
										case 0x8053:
											if (nowmap() == 0x44d)
											{
												regI1 = -1;
											}
											break;
										case 0x8054:
											if (nowmap() == 0x2ef)
											{
												regI1 = -1;
											}
											break;
										case 0x8055:
											if (nowmap() == 0x300)
											{
												regI1 = -1;
											}
											break;
										case 0x8056:
											if (nowmap() == 0x2e6)
											{
												regI1 = -1;
											}
											break;
										case 0x8057:
											if (nowmap() == 129)
											{
												regI1 = -1;
											}
											break;
										case 0x8058:
											if (nowmap() == 0x15e)
											{
												regI1 = -1;
											}
											break;
										case 0x8059:
											if (nowmap() == 0x47f)
											{
												regI1 = -1;
											}
											break;
										case 0x805a:
											if (nowmap() == 0x182)
											{
												regI1 = -1;
											}
											break;
										case 0x805b:
											if (nowmap() == 0x1e8)
											{
												regI1 = -1;
											}
											break;
										default:
											if (nowmap() == 142)
											{
												regI1 = -1;
											}
											break;
									}
									if (regI1 != -1)
									{
										file_var_25 = 0;
										if (getsummonmodebit())
										{
											setmeswincaptionid(0, 2);
											setkutipakustatus(1);
											setunazukistatus(1);
											askpos(0, 1, 127);
											file_var_25 = aaske(0, 0x1000006);
											mesclose(0);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
										}
										if (getchocobomodebit())
										{
											setmeswincaptionid(0, 2);
											setkutipakustatus(1);
											setunazukistatus(1);
											askpos(0, 1, 127);
											file_var_25 = aaske(0, 0x1000007);
											mesclose(0);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
										}
										if (file_var_25 == 0)
										{
											file_var_24 = regI1;
											sysReq(1, telepoeffectctrl::telepoeffect);
										}
										else
										{
											regI1 = 0;
										}
									}
								}
								switch (regI1)
								{
									case -1:
										setmeswincaptionid(0, 2);
										amese(0, 0x1000008);
										messync(0, 1);
										mesclose(0);
										break;
									case 0:
										break;
								}
							}
							break;
						default:
							break;
					}
				}
				else
				{
					setmeswincaptionid(0, 2);
					askpos(0, 0, 1);
					local_var_3d = aaske(0, 0x1000009);
					mesclose(0);
					switch (local_var_3d)
					{
						case 0:
							if (scratch1_var_2c)
							{
								if (nowmap() == 0x309)
								{
									setmeswincaptionid(0, 3);
									ames(0, 0x1000002, 0x3c0, 0x21c, 4);
								}
								else
								{
									setmeswincaptionid(0, 2);
									amese(0, 0x1000003);
								}
								messync(0, 1);
								mesclose(0);
							}
							messync(0, 1);
							local_var_40 = nowjumpindex();
							if ((nowmap() == 0x193 && global_flag[0] < 0x541))
							{
								healall(0);
							}
							else
							{
								setnowjumpindex(-2);
							}
							fadelayer(5);
							fadeout(10);
							wait(10);
							openfullscreenmenu_48d(0x8005, 0);
							fadein(10);
							wait(10);
							setnowjumpindex(local_var_40);
							break;
						default:
							break;
					}
				}
			}
			else if (getsaveramtelepostatus())
			{
				fs_stonewinopen(haveitem(0x2000));
				setmeswincaptionid(0, 2);
				askpos(0, 0, 1);
				local_var_3d = aaske(0, 0x100000a);
				mesclose(0);
				fs_stonewinclose();
				messync(0, 1);
				switch (local_var_3d)
				{
					case 0:
						if (haveitem(0x2000) == 0)
						{
							setmeswincaptionid(0, 2);
							setmesmacro(0, 0, 1, 0x2000);
							amese(0, 0x1000004);
							messync(0, 1);
							mesclose(0);
						}
						else
						{
							fs_stonewinopen(haveitem(0x2000));
							regI2 = 0;
							for (regI0 = 0; regI0 < 25; regI0 = (regI0 + 1))
							{
								if (g_menu_telepo_dest[regI0] != 0)
								{
									setmesmacro(0, regI0, 1, g_menu_telepo_dest[regI0]);
									regI2 = (regI2 + 1);
								}
								else
								{
									setaskselectignore(0, regI0);
								}
							}
							if (regI2 > 6)
							{
								regI3 = 7;
							}
							else
							{
								regI3 = (regI2 + 1);
							}
							setmeswinline(0, regI3);
							askpos(0, 0, regI2);
							local_var_3d = aask(0, 0x1000005, 120, 0x3fe, 1);
							fs_stonewinclose();
							if (local_var_3d >= 25)
							{
								regI1 = 0;
							}
							else
							{
								regI1 = g_menu_telepo_dest[local_var_3d];
								switch (regI1)
								{
									case 0x8040:
										if (nowmap() == 0x132)
										{
											regI1 = -1;
										}
										break;
									case 0x8042:
										if (nowmap() == 0x276)
										{
											regI1 = -1;
										}
										break;
									case 0x8043:
										if (nowmap() == 66)
										{
											regI1 = -1;
										}
										break;
									case 0x8044:
										if (nowmap() == 157)
										{
											regI1 = -1;
										}
										break;
									case 0x8045:
										if (nowmap() == 176)
										{
											regI1 = -1;
										}
										break;
									case 0x8047:
										if (nowmap() == 0x2f4)
										{
											regI1 = -1;
										}
										break;
									case 0x8048:
										if (nowmap() == 188)
										{
											regI1 = -1;
										}
										break;
									case 0x8049:
										if (nowmap() == 0x2ce)
										{
											regI1 = -1;
										}
										break;
									case 0x804a:
										if (nowmap() == 0x317)
										{
											regI1 = -1;
										}
										break;
									case 0x804b:
										if (nowmap() == 0x168)
										{
											regI1 = -1;
										}
										break;
									case 0x804c:
										if (nowmap() == 0x193)
										{
											regI1 = -1;
										}
										break;
									case 0x804e:
										if (nowmap() == 0x1c2)
										{
											regI1 = -1;
										}
										break;
									case 0x8050:
										if (nowmap() == 0x2ae)
										{
											regI1 = -1;
										}
										break;
									case 0x8051:
										if (nowmap() == 0x252)
										{
											regI1 = -1;
										}
										break;
									case 0x8052:
										if (nowmap() == 0x337)
										{
											regI1 = -1;
										}
										break;
									case 0x8053:
										if (nowmap() == 0x44d)
										{
											regI1 = -1;
										}
										break;
									case 0x8054:
										if (nowmap() == 0x2ef)
										{
											regI1 = -1;
										}
										break;
									case 0x8055:
										if (nowmap() == 0x300)
										{
											regI1 = -1;
										}
										break;
									case 0x8056:
										if (nowmap() == 0x2e6)
										{
											regI1 = -1;
										}
										break;
									case 0x8057:
										if (nowmap() == 129)
										{
											regI1 = -1;
										}
										break;
									case 0x8058:
										if (nowmap() == 0x15e)
										{
											regI1 = -1;
										}
										break;
									case 0x8059:
										if (nowmap() == 0x47f)
										{
											regI1 = -1;
										}
										break;
									case 0x805a:
										if (nowmap() == 0x182)
										{
											regI1 = -1;
										}
										break;
									case 0x805b:
										if (nowmap() == 0x1e8)
										{
											regI1 = -1;
										}
										break;
									default:
										if (nowmap() == 142)
										{
											regI1 = -1;
										}
										break;
								}
								if (regI1 != -1)
								{
									file_var_25 = 0;
									if (getsummonmodebit())
									{
										setmeswincaptionid(0, 2);
										setkutipakustatus(1);
										setunazukistatus(1);
										askpos(0, 1, 127);
										file_var_25 = aaske(0, 0x1000006);
										mesclose(0);
										messync(0, 1);
										setkutipakustatus(0);
										setunazukistatus(0);
									}
									if (getchocobomodebit())
									{
										setmeswincaptionid(0, 2);
										setkutipakustatus(1);
										setunazukistatus(1);
										askpos(0, 1, 127);
										file_var_25 = aaske(0, 0x1000007);
										mesclose(0);
										messync(0, 1);
										setkutipakustatus(0);
										setunazukistatus(0);
									}
									if (file_var_25 == 0)
									{
										file_var_24 = regI1;
										sysReq(1, telepoeffectctrl::telepoeffect);
									}
									else
									{
										regI1 = 0;
									}
								}
							}
							switch (regI1)
							{
								case -1:
									setmeswincaptionid(0, 2);
									amese(0, 0x1000008);
									messync(0, 1);
									mesclose(0);
									break;
								case 0:
									break;
							}
						}
						break;
					default:
						break;
				}
			}
			else
			{
				setmeswincaptionid(0, 2);
				amese(0, 0x100000b);
				messync(0, 1);
				mesclose(0);
			}
		}
		else if (local_var_3b != 255)
		{
			if (getsaveramsavestatus())
			{
				askpos(0, 0, 1);
				setmeswincaptionid(0, 2);
				local_var_3d = aaske(0, 0x100000c);
				mesclose(0);
				switch (local_var_3d)
				{
					case 0:
						if (scratch1_var_2c)
						{
							if (nowmap() == 0x309)
							{
								setmeswincaptionid(0, 3);
								ames(0, 0x1000002, 0x3c0, 0x21c, 4);
							}
							else
							{
								setmeswincaptionid(0, 2);
								amese(0, 0x1000003);
							}
							messync(0, 1);
							mesclose(0);
						}
						messync(0, 1);
						local_var_40 = nowjumpindex();
						if ((nowmap() == 0x193 && global_flag[0] < 0x541))
						{
							healall(0);
						}
						else
						{
							setnowjumpindex(-2);
						}
						fadelayer(5);
						fadeout(10);
						wait(10);
						openfullscreenmenu_48d(0x8005, 0);
						fadein(10);
						wait(10);
						setnowjumpindex(local_var_40);
						break;
					default:
						break;
				}
			}
			else
			{
				setmeswincaptionid(0, 2);
				amese(0, 0x100000d);
				messync(0, 1);
				mesclose(0);
			}
		}
		else
		{
			wait(30);
		}
		if (file_var_24 < 1)
		{
			wait(1);
			sysReqew(1, crystaldummyPC::クリスタルリリース);
			wait(1);
			resumebattle();
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			sethpmenu(1);
		}
		file_var_26 = 0;
		return;
	}


	function showoff()
	{
		reqdisable(2);
		reqdisable(8);
		reqdisable(13);
		usecharhit(0);
		show_38(0);
		return;
	}


	function showon()
	{
		show_38(1);
		usecharhit(1);
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}
}


script crystaldummyPC(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		while (true)
		{
			switch (nowmap())
			{
				case 176:
					if (g_btl_クリスタルミミックレッド == 2)
					{
						if (ismapjumpgroupflag(0x165) == 0)
						{
							setmapjumpgroupflag(0x165);
						}
					}
					else if (scratch1_var_2d)
					{
						if (ismapjumpgroupflag(0x165) == 1)
						{
							releasemapjumpgroupflag(0x165);
						}
					}
					else if (ismapjumpgroupflag(0x165) == 0)
					{
						setmapjumpgroupflag(0x165);
					}
					break;
				case 186:
					if (g_btl_クリスタルミミックブルー == 2)
					{
						if (ismapjumpgroupflag(0x164) == 0)
						{
							setmapjumpgroupflag(0x164);
						}
					}
					else if (scratch1_var_2d)
					{
						if (ismapjumpgroupflag(0x164) == 1)
						{
							releasemapjumpgroupflag(0x164);
						}
					}
					else if (ismapjumpgroupflag(0x164) == 0)
					{
						setmapjumpgroupflag(0x164);
					}
					break;
				case 0x260:
					if (g_btl_クリスタルミミックグリーン == 2)
					{
						if (ismapjumpgroupflag(127) == 0)
						{
							setmapjumpgroupflag(127);
						}
						if (ismapjumpgroupflag(128) == 1)
						{
							releasemapjumpgroupflag(128);
						}
					}
					else if (scratch1_var_2d)
					{
						if (ismapjumpgroupflag(127) == 1)
						{
							releasemapjumpgroupflag(127);
						}
						if (ismapjumpgroupflag(128) == 1)
						{
							releasemapjumpgroupflag(128);
						}
					}
					else
					{
						if (ismapjumpgroupflag(128) == 0)
						{
							setmapjumpgroupflag(128);
						}
						if (ismapjumpgroupflag(127) == 1)
						{
							releasemapjumpgroupflag(127);
						}
					}
					break;
			}
			wait(1);
		}
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_42;            // pos: 0x0;
	float   local_var_43;            // pos: 0x4;
	float   local_var_44;            // pos: 0x8;

	function クリスタルタッチ()
	{
		capturepc(-1);
		system_crystal_actor.getpos(&local_var_42, &local_var_43, &local_var_44);
		turn(local_var_42, local_var_43, local_var_44);
		turnsync();
		setforcerelax(1);
		return;
	}


	function クリスタルビュー()
	{
		capturepc(-1);
		system_crystal_actor.getpos(&local_var_42, &local_var_43, &local_var_44);
		turn(local_var_42, local_var_43, local_var_44);
		setforcerelax(0);
		turnsync();
		return;
	}


	function クリスタルリリース()
	{
		motionsync_282(1);
		clearforcerelax();
		releasepc();
		return;
	}
}

symlink NPC02(NPC01) : 0x1;

symlink NPC03(NPC01) : 0x2;

symlink NPC04(NPC01) : 0x3;

symlink NPC05(NPC01) : 0x4;

symlink NPC06(NPC01) : 0x5;

symlink NPC07(NPC01) : 0x6;

symlink NPC08(NPC01) : 0x7;

symlink NPC09(NPC01) : 0x8;

symlink NPC10(NPC01) : 0x9;

symlink NPC11(NPC01) : 0xa;

symlink NPC12(NPC01) : 0xb;

symlink NPC13(NPC01) : 0xc;

symlink NPC14(NPC01) : 0xd;

symlink NPC15(NPC01) : 0xe;

symlink NPC16(NPC01) : 0xf;

symlink NPC17(NPC01) : 0x10;

symlink モグシーズ０２(モグシーズ０１) : 0x1;

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_21;             // pos: 0x31;


script NPC01(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_45;            // pos: 0x0;

	function init()
	{
		local_var_45 = getduplicateid();
		return;
	}


	function main(1)
	{
		return;
	}


	function talk(2)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_47;            // pos: 0xd;
	float   local_var_48[15];        // pos: 0x10;
	float   local_var_49[5];         // pos: 0x4c;
	float   local_var_4b;            // pos: 0x64;
	float   local_var_4c[4];         // pos: 0x68;
	float   local_var_4f;            // pos: 0x80;
	int     local_var_52;            // pos: 0x9c;
	u_char  local_var_54;            // pos: 0xa4;
	u_char  local_var_56;            // pos: 0xa6;
	u_char  local_var_57;            // pos: 0xa7;
	u_char  local_var_58;            // pos: 0xa8;

	function NPC配置()
	{
		switch (local_var_45)
		{
			case 0:
				local_var_47 = 9;
				local_var_57 = 1;
				local_var_58 = 0;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 130.657242;
				local_var_48[1] = -10;
				local_var_48[2] = 164.055832;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = -1.646047;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 130.657242;
				local_var_4c[1] = -10;
				local_var_4c[2] = 164.055832;
				local_var_4c[3] = -1.646047;
				break;
			case 1:
				local_var_47 = 10;
				local_var_57 = 1;
				local_var_58 = 0;
				local_var_54 = 0;
				local_var_56 = 3;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 142.154968;
				local_var_48[1] = -10;
				local_var_48[2] = 163.907684;
				local_var_48[3] = 138.16275;
				local_var_48[4] = -10;
				local_var_48[5] = 165.744431;
				local_var_48[6] = 142.14563;
				local_var_48[7] = -10;
				local_var_48[8] = 168.280167;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = -0.961483002;
				local_var_49[1] = -1.39426506;
				local_var_49[2] = 1.45401394;
				local_var_49[3] = 0;
				local_var_4c[0] = 142.154968;
				local_var_4c[1] = -10;
				local_var_4c[2] = 163.907684;
				local_var_4c[3] = -0.961483002;
				break;
			case 2:
				local_var_47 = 10;
				local_var_57 = 1;
				local_var_58 = 0;
				local_var_54 = 1;
				local_var_56 = 2;
				local_var_52 = 70;
				local_var_4f = 0;
				local_var_48[0] = 133.415558;
				local_var_48[1] = -10;
				local_var_48[2] = 168.534927;
				local_var_48[3] = 133.445557;
				local_var_48[4] = -10;
				local_var_48[5] = 163.360168;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = -1.59212899;
				local_var_49[1] = -0.655382991;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 133.415558;
				local_var_4c[1] = -10;
				local_var_4c[2] = 168.534927;
				local_var_4c[3] = -1.59212899;
				break;
			case 3:
				local_var_47 = 8;
				local_var_57 = 3;
				local_var_58 = 0;
				local_var_54 = 0;
				local_var_56 = 1;
				local_var_52 = 0;
				local_var_4f = 2;
				local_var_48[0] = 128.161987;
				local_var_48[1] = -10;
				local_var_48[2] = 157.734756;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 1.27184498;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 128.161987;
				local_var_4c[1] = -10;
				local_var_4c[2] = 157.734756;
				local_var_4c[3] = 1.27184498;
				break;
			case 4:
				local_var_47 = 7;
				local_var_57 = 5;
				local_var_58 = 2;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 131.898956;
				local_var_48[1] = -10;
				local_var_48[2] = 173.105469;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 2.88862801;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 131.898956;
				local_var_4c[1] = -10;
				local_var_4c[2] = 173.105469;
				local_var_4c[3] = 2.88862801;
				break;
			case 5:
				local_var_47 = 8;
				local_var_57 = 4;
				local_var_58 = 1;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 114.058296;
				local_var_48[1] = -10;
				local_var_48[2] = 169.169159;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = -0.355879992;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 114.058296;
				local_var_4c[1] = -10;
				local_var_4c[2] = 169.169159;
				local_var_4c[3] = -0.355879992;
				break;
			case 6:
				local_var_47 = 1;
				local_var_57 = 4;
				local_var_58 = 2;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 114.099907;
				local_var_48[1] = -10;
				local_var_48[2] = 170.82988;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = -2.65517092;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 114.099907;
				local_var_4c[1] = -10;
				local_var_4c[2] = 170.82988;
				local_var_4c[3] = -2.65517092;
				break;
			case 7:
				local_var_47 = 7;
				local_var_57 = 3;
				local_var_58 = 1;
				local_var_54 = 0;
				local_var_56 = 1;
				local_var_52 = 0;
				local_var_4f = 1.70000005;
				local_var_48[0] = 128.178024;
				local_var_48[1] = -10;
				local_var_48[2] = 148.210052;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 1.08755803;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 128.178024;
				local_var_4c[1] = -10;
				local_var_4c[2] = 148.210052;
				local_var_4c[3] = 1.08755803;
				break;
			case 8:
				local_var_47 = 11;
				local_var_57 = 3;
				local_var_58 = 4;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 131.476517;
				local_var_48[1] = -9.45000839;
				local_var_48[2] = 168.675644;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 1.52665603;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 131.476517;
				local_var_4c[1] = -9.45000839;
				local_var_4c[2] = 168.675644;
				local_var_4c[3] = 1.52665603;
				break;
			case 9:
				local_var_47 = 3;
				local_var_57 = 3;
				local_var_58 = 1;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 133.877869;
				local_var_48[1] = -10;
				local_var_48[2] = 161.161316;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 0.590272009;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 133.877869;
				local_var_4c[1] = -10;
				local_var_4c[2] = 161.161316;
				local_var_4c[3] = 0.590272009;
				break;
			case 10:
				local_var_47 = 3;
				local_var_57 = 1;
				local_var_58 = 1;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 134.750961;
				local_var_48[1] = -10;
				local_var_48[2] = 161.525253;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 0.205980003;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 134.750961;
				local_var_4c[1] = -10;
				local_var_4c[2] = 161.525253;
				local_var_4c[3] = 0.205980003;
				break;
			case 11:
				local_var_47 = 1;
				local_var_57 = 3;
				local_var_58 = 0;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 132.947418;
				local_var_48[1] = -10;
				local_var_48[2] = 161.399994;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 0.271551996;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 132.947418;
				local_var_4c[1] = -10;
				local_var_4c[2] = 161.399994;
				local_var_4c[3] = 0.271551996;
				break;
			case 12:
				local_var_47 = 2;
				local_var_57 = 4;
				local_var_58 = 0;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 112.769516;
				local_var_48[1] = -10;
				local_var_48[2] = 170.19751;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 1.46400201;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 112.769516;
				local_var_4c[1] = -10;
				local_var_4c[2] = 170.19751;
				local_var_4c[3] = 1.46400201;
				break;
			case 13:
				local_var_47 = 7;
				local_var_57 = 4;
				local_var_58 = 2;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 114.401375;
				local_var_48[1] = -10;
				local_var_48[2] = 161.590088;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = -0.531090975;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 114.401375;
				local_var_4c[1] = -10;
				local_var_4c[2] = 161.590088;
				local_var_4c[3] = -0.531090975;
				break;
			case 14:
				local_var_47 = 7;
				local_var_57 = 5;
				local_var_58 = 3;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 112.996063;
				local_var_48[1] = -10;
				local_var_48[2] = 163.868057;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = -2.68338203;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 112.996063;
				local_var_4c[1] = -10;
				local_var_4c[2] = 163.868057;
				local_var_4c[3] = -2.68338203;
				break;
			case 15:
				local_var_47 = 3;
				local_var_57 = 1;
				local_var_58 = 0;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 129.647018;
				local_var_48[1] = -10;
				local_var_48[2] = 169.292221;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 1.82184505;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 129.647018;
				local_var_4c[1] = -10;
				local_var_4c[2] = 169.292221;
				local_var_4c[3] = 1.82184505;
				break;
			case 16:
				local_var_47 = 0;
				local_var_57 = 1;
				local_var_58 = 0;
				local_var_54 = 0;
				local_var_56 = 0;
				local_var_52 = 0;
				local_var_4f = 0;
				local_var_48[0] = 115.960815;
				local_var_48[1] = -10;
				local_var_48[2] = 147.198334;
				local_var_48[3] = 0;
				local_var_48[4] = 0;
				local_var_48[5] = 0;
				local_var_48[6] = 0;
				local_var_48[7] = 0;
				local_var_48[8] = 0;
				local_var_48[9] = 0;
				local_var_48[10] = 0;
				local_var_48[11] = 0;
				local_var_49[0] = 0.890586019;
				local_var_49[1] = 0;
				local_var_49[2] = 0;
				local_var_49[3] = 0;
				local_var_4c[0] = 115.960815;
				local_var_4c[1] = -10;
				local_var_4c[2] = 147.198334;
				local_var_4c[3] = 0.890586019;
				break;
		}
		hidecomplete();
		usemapid(0);
		setpos(local_var_48[0], local_var_48[1], local_var_48[2]);
		dir(local_var_49[0]);
		switch (local_var_47)
		{
			case 10:
				bindp2_d4(0x300000a, local_var_57, local_var_58);
				break;
			case 1:
				bindp2_d4(0x3000004, local_var_57, local_var_58);
				break;
			case 2:
				bindp2_d4(0x3000005, local_var_57, local_var_58);
				break;
			case 3:
				bindp2_d4(0x3000008, local_var_57, local_var_58);
				break;
			case 7:
				bindp2_d4(0x3000006, local_var_57, local_var_58);
				break;
			case 8:
				bindp2_d4(0x3000007, local_var_57, local_var_58);
				break;
			case 9:
				bindp2_d4(0x300000b, local_var_57, local_var_58);
				break;
			case 11:
				bindp2_d4(0x300000c, local_var_57, local_var_58);
				break;
			case 0:
				bindp2_d4(0x300000d, local_var_57, local_var_58);
				break;
		}
		if (local_var_47 == 9)
		{
			stdmotionread(17);
		}
		else if (local_var_47 == 10)
		{
			stdmotionread(1);
		}
		else
		{
			stdmotionread(16);
		}
		set_ignore_hitgroup(1);
		setreachr(0.300000012);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		local_var_4b = getdefaultwalkspeed();
		switch (local_var_45)
		{
			case 0:
				setnpcname(52);
				reqenable(2);
				break;
			case 3:
				if ((g_iw_便利フラグ２[0] & 2))
				{
					setnpcname(0x3de);
				}
				else
				{
					setnpcname(42);
				}
				reqenable(2);
				break;
			case 4:
				setnpcname(5);
				reqenable(2);
				break;
			case 6:
				setnpcname(170);
				reqenable(2);
				break;
			case 7:
				setnpcname(94);
				reqenable(2);
				break;
			case 16:
				setnpcname(184);
				reqenable(2);
				break;
			default:
				reqdisable(2);
				break;
		}
		usecharhit(1);
		setweight(-1);
		fetchambient();
		//Couldn't get labels for REQ because either script or function is not immediate
		sysReq(1, getmyid(), 4);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_4a;            // pos: 0x60;
	float   local_var_4d;            // pos: 0x78;
	float   local_var_4e;            // pos: 0x7c;
	float   local_var_50;            // pos: 0x84;
	int     local_var_51;            // pos: 0x98;
	int     local_var_53;            // pos: 0xa0;
	u_char  local_var_55;            // pos: 0xa5;

	function NPC挙動()
	{
		local_var_55 = 0;
		local_var_51 = 0;
		local_var_53 = 0;
		local_var_4d = 0.00600000005;
		local_var_4e = 0.00300000003;
		switch (local_var_45)
		{
			case 8:
			case 9:
			case 12:
			case 14:
				break;
			default:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				break;
		}
		switch (local_var_45)
		{
			case 1:
			case 2:
			case 3:
			case 7:
				setkubifuristatus(1);
				break;
			case 4:
				while (true)
				{
					switch ((rand_29(100) % 2))
					{
						case 0:
							motionplay_bb(0x10000000, 20);
							break;
						default:
							stdmotionplay(0x1000015);
							break;
					}
					wait(1);
					motionsync_282(1);
					wait((30 + rand_29(15)));
				}
			case 5:
				while (true)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					switch ((rand_29(100) % 2))
					{
						case 0:
							sysLookata(NPC07);
							break;
						default:
							sysLookata(NPC13);
							break;
					}
					switch ((rand_29(100) % 2))
					{
						case 0:
							stdmotionplay(0x1000010);
							break;
						default:
							stdmotionplay(0x1000016);
							break;
					}
					wait(1);
					motionsync_282(1);
					setkutipakustatus(0);
					setunazukistatus(0);
					wait((30 + rand_29(15)));
				}
			case 6:
				while (true)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					switch ((rand_29(100) % 2))
					{
						case 0:
							sysLookata(NPC06);
							break;
						default:
							sysLookata(NPC13);
							break;
					}
					switch ((rand_29(100) % 2))
					{
						case 0:
							motionplay_bb(0x10000001, 20);
							break;
						default:
							stdmotionplay(0x1000016);
							break;
					}
					wait(1);
					motionsync_282(1);
					setkutipakustatus(0);
					setunazukistatus(0);
					wait((30 + rand_29(15)));
				}
			case 8:
				setradius_221(0.200000003, 0.699999988);
				setautorelax(0);
				motionstartframe(0);
				motionloopframe(0, -1);
				motionloop(1);
				motionplay_bb(0x10000002, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				return;
			case 9:
				stdmotionread(18);
				stdmotionreadsync();
				stdmotionvariation(1);
				stdmotionplay_2c2(0x1000000, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				while (true)
				{
					sysLookata(NPC02);
					setkutipakustatus(1);
					setunazukistatus(1);
					wait((60 + rand_29(15)));
					sysLookata(NPC11);
					setkutipakustatus(1);
					setunazukistatus(1);
					wait((60 + rand_29(15)));
				}
			case 10:
				while (true)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					sysLookata(NPC10);
					stdmotionplay(0x1000013);
					wait(1);
					motionsync_282(1);
					setkutipakustatus(0);
					setunazukistatus(0);
					wait(5);
					setkutipakustatus(1);
					setunazukistatus(1);
					sysLookata(NPC03);
					stdmotionplay(0x1000010);
					wait(1);
					motionsync_282(1);
					setkutipakustatus(0);
					setunazukistatus(0);
					wait(5);
				}
			case 11:
				while (true)
				{
					sysLookata(NPC02);
					wait((60 + rand_29(60)));
					sysLookata(NPC03);
					wait((60 + rand_29(60)));
				}
			case 12:
				setautorelax(0);
				motionstartframe(0);
				motionloopframe(0, -1);
				motionloop(1);
				motionplay_bb(0x10000003, 0);
				setkutipakustatus(1);
				setunazukistatus(1);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				while (true)
				{
					sysLookata(NPC06);
					wait((60 + rand_29(60)));
					sysLookata(NPC07);
					wait((60 + rand_29(60)));
				}
			case 13:
				while (true)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					lookat((getrotangzx() - 0.52359879));
					stdmotionplay(0x1000013);
					wait(1);
					motionsync_282(1);
					setkutipakustatus(0);
					setunazukistatus(0);
					wait(5);
					setkutipakustatus(1);
					setunazukistatus(1);
					lookat((getrotangzx() + 0.52359879));
					stdmotionplay(0x1000015);
					wait(1);
					motionsync_282(1);
					setkutipakustatus(0);
					setunazukistatus(0);
					wait(5);
				}
			case 14:
				setkutipakustatus(1);
				setunazukistatus(1);
				stdmotionread(18);
				stdmotionreadsync();
				stdmotionvariation(1);
				stdmotionplay_2c2(0x1000000, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				sysLookatab(NPC14, 13);
				return;
			case 16:
				setkubifuristatus(1);
				setautorelax(0);
				motionstartframe(0);
				motionloopframe(0, -1);
				motionloop(1);
				motionplay_bb(0x10000004, 0);
				bindc(1, 0x300000e, 2);
				setkubifuristatus(1);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				return;
		}
		switch (local_var_45)
		{
			case 0:
				return;
		}
		switch (local_var_56)
		{
			case 0:
				usemapid(0);
				setweight(-1);
				while (true)
				{
					stdmotionplay(0x1000002);
					wait(30);
					motiontrigger();
					motionsync_282(1);
					stdmotionplay(0x1000000);
					local_var_55 = (local_var_55 + 1);
					if (local_var_55 >= 4)
					{
						local_var_55 = 0;
					}
					if (local_var_54 != 0)
					{
						setaturnlookatlockstatus(1);
						aturny((sin(local_var_49[local_var_55]) + getx()), (cos(local_var_49[local_var_55]) + getz()));
						wait(local_var_52);
					}
					else
					{
						wait((rand_29(30) + 30));
					}
				}
			case 1:
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4d = 0.00600000005;
				local_var_4e = 0.00300000003;
				motioncancel();
				stdmotionplay(0x1000000);
				while (true)
				{
					setreachr(0.100000001);
					local_var_50 = ((((local_var_4f - 1) * 0.899999976) + (((local_var_4f - 1) * (rand_29(100) % 10)) / 100)) + 1);
					local_var_49[1] = rand_29(100);
					local_var_49[1] = (local_var_49[1] / 100);
					switch ((rand_29(100) % 2))
					{
						case 0:
							local_var_49[0] = normalang(((local_var_49[0] + 1.57079637) + (1.57079637 * local_var_49[1])));
							break;
						default:
							local_var_49[0] = normalang(((local_var_49[0] - 1.57079637) - (1.57079637 * local_var_49[1])));
							break;
					}
					local_var_48[0] = (local_var_4c[0] + (sin(local_var_49[0]) * local_var_50));
					local_var_48[1] = local_var_4c[1];
					local_var_48[2] = (local_var_4c[2] + (cos(local_var_49[0]) * local_var_50));
					local_var_4a = ((getlength3(local_var_48[0], local_var_48[1], local_var_48[2]) / local_var_4b) + 1);
					setwalkspeed(local_var_4b);
					stdmotionplay_2c2(0x1000000, 20);
					rmove(local_var_48[0], local_var_48[1], local_var_48[2]);
					local_var_51 = 0;
					while (!(local_var_51 >= local_var_4a))
					{
						local_var_51 = (local_var_51 + 1);
						wait(1);
					}
					wait((rand_29(50) + 30));
					wait(1);
				}
			default:
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4d = 0.00600000005;
				local_var_4e = 0.00300000003;
				stdmotionplay(0x1000000);
				setpos(local_var_48[((local_var_56 - 1) * 3)], local_var_48[(((local_var_56 - 1) * 3) + 1)], local_var_48[(((local_var_56 - 1) * 3) + 2)]);
				while (true)
				{
					setreachr(0.100000001);
					if (local_var_4b <= 0)
					{
						wait(1);
					}
					else
					{
						setwalkspeed(local_var_4b);
						stdmotionplay_2c2(0x1000000, 20);
						move(local_var_48[local_var_55], local_var_48[(local_var_55 + 1)], local_var_48[(local_var_55 + 2)]);
						if (local_var_54 != 0)
						{
							wait(15);
							setaturnlookatlockstatus(1);
							aturny((sin(local_var_49[(local_var_55 / 3)]) + getx()), (cos(local_var_49[(local_var_55 / 3)]) + getz()));
							wait(local_var_52);
						}
						local_var_55 = (local_var_55 + 3);
						if (local_var_55 >= (local_var_56 * 3))
						{
							local_var_55 = 0;
						}
						wait(1);
					}
				}
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_46;            // pos: 0x5;
	u_char	weatherAnswer;

	function talk_step01()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		switch (local_var_45)
		{
			case 3:
				setkubifuristatus(0);
				if ((シナリオフラグ >= 0x622 && mp_sav初回 != 0))
				{
					if (!((g_iw_便利フラグ２[0] & 2)))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000019);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						g_iw_便利フラグ２[0] = (g_iw_便利フラグ２[0] | 2);
						setnpcname(0x3de);
					}
					else
					{
						#include "include/calculation.c"
						mp_sav季節 = ((getplayloophour() - mp_sav補正時間) % 3);
						if (mp_sav季節 != 0)
						{
							global_var_17 = getplayloopmin();
							if (mp_sav季節 >= 2)
							{
								mp_sav季節 = 1;
								switch (global_var_17)
								{
									case between(0, 47):
										mp_giza_rains = 1;
										break;
									case between(48, 59):
										mp_giza_rains = 2;
										break;
								}
							}
							else
							{
								mp_sav季節 = 1;
								switch (global_var_17)
								{
									case between(0, 18):
										mp_giza_rains = 0;
										break;
									case between(18, 59):
										mp_giza_rains = 1;
										break;
								}
							}
						}
						else
						{
							mp_sav季節 = 0;
							file_var_21 = getplayloopmin();
							switch (file_var_21)
							{
								case between(0, 9):
									mp_giza_rains = 0;
									break;
								case between(10, 47):
									mp_giza_rains = 1;
									break;
								case between(48, 59):
									mp_giza_rains = 2;
									break;
							}
						}
						gizaWeatherChange = 0;
						if (mp_sav季節 == 1)
						{
							switch (mp_giza_rains)
							{
								case 0:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001a);
									messync(0, 1);
									askpos(0, 0, 1);
									weatherAnswer = aaske(0, 0x1000048);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
								case 1:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001b);
									messync(0, 1);
									askpos(0, 0, 1);
									weatherAnswer = aaske(0, 0x1000048);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
								case 2:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001c);
									messync(0, 1);
									askpos(0, 0, 1);
									weatherAnswer = aaske(0, 0x1000048);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							if (weatherAnswer == 0)
							{
								gizaWeatherChange = 2;
								fadeout(15);
								fadesync();
								hidemapmodel(5);
								showmapmodel(6);
								unkCall_5c3(1);
								fadein(15);
								fadesync();
							}
						}
						else
						{
							switch (mp_giza_rains)
							{
								case 0:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001d);
									messync(0, 1);
									askpos(0, 0, 1);
									weatherAnswer = aaske(0, 0x1000049);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
								case 1:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001e);
									messync(0, 1);
									askpos(0, 0, 1);
									weatherAnswer = aaske(0, 0x1000049);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
								case 2:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001f);
									messync(0, 1);
									askpos(0, 0, 1);
									weatherAnswer = aaske(0, 0x1000049);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							if (weatherAnswer == 0)
							{
								gizaWeatherChange = 1;
								fadeout(15);
								fadesync();
								showmapmodel(5);
								hidemapmodel(6);
								unkCall_5c3(0);
								fadein(15);
								fadesync();
							}
						}
					}
				}
				else
				{
					switch (シナリオフラグ)
					{
						case lte(85):
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000020);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000020);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						case lte(0x157):
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000020);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000020);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						case lte(0x5f0):
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000021);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000021);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						case lte(0x1004):
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000022);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000022);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						default:
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000023);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x1000023);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
					}
				}
				setkubifuristatus(1);
				break;
			case 4:
				if (!((g_iw_便利フラグ[0] & 128)))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000024);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					g_iw_便利フラグ[0] = (g_iw_便利フラグ[0] | 128);
				}
				else
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000025);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				break;
			case 6:
				switch (シナリオフラグ)
				{
					case lte(85):
						switch (local_var_46)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000026);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_46 = (local_var_46 + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000026);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x157):
						switch (local_var_46)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000026);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_46 = (local_var_46 + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000026);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x5f0):
						switch (local_var_46)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000027);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_46 = (local_var_46 + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000027);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x1004):
						switch (local_var_46)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000028);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_46 = (local_var_46 + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000028);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					default:
						switch (local_var_46)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000029);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_46 = (local_var_46 + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000029);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
				}
				break;
			case 7:
				setkubifuristatus(0);
				if ((((0xa00000 | getquestscenarioflag(160)) >= 0xa00078 && (0xa00000 | getquestscenarioflag(160)) < 0xa0008c) && global_var_20 == 0))
				{
					switch (local_var_46)
					{
						case lte(0):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100002a);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							local_var_46 = (local_var_46 + 1);
							break;
						default:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100002a);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
					}
				}
				else
				{
					switch (シナリオフラグ)
					{
						case lte(85):
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002b);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002b);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						case lte(0x157):
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002b);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002b);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						case lte(0x5f0):
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002c);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002c);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						case lte(0x1004):
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002d);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002d);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						default:
							switch (local_var_46)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002e);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_46 = (local_var_46 + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100002e);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
					}
				}
				setkubifuristatus(1);
				break;
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_59;            // pos: 0xac;
	u_char  local_var_5a;            // pos: 0xb0;
	u_char  local_var_5b;            // pos: 0xb1;
	char    local_var_5c;            // pos: 0xb2;
	int     local_var_5d;            // pos: 0xb4;
	int     local_var_5e;            // pos: 0xb8;

	function talk_gardy()
	{
		#include "include/calculation.c"
		if (mp_sav季節 == 1)
		{
			local_var_59 = 236;
		}
		else
		{
			local_var_59 = 253;
		}
		reqdisable(16);
		ucoff();
		wait(1);
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		stdmotionplay(0x1000002);
		sysTurna(-1);
		local_var_5b = 0;
		local_var_5c = -1;
		local_var_5e = getprice(0xb000);
		gillwinopen(havegill());
		setmesmacro(0, 0, 0, local_var_5e);
		askpos(0, 0, 1);
		if (!((quest_global_flag[4] & 2)))
		{
			local_var_5a = aaske(0, 0x100002f);
			quest_global_flag[4] = (quest_global_flag[4] | 2);
		}
		else
		{
			local_var_5a = aaske(0, 0x1000030);
		}
		mesclose(0);
		messync(0, 1);
		switch (local_var_5a)
		{
			case 0:
				if (getchocobomodebit())
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000031);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				}
				local_var_5d = checkgill(local_var_5e);
				if (local_var_5d != 1)
				{
					amese(0, 0x1000032);
					messync(0, 0);
				}
				else
				{
					if (getsummonmode())
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000033);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						break;
					}
					if (getsummonmodebit())
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						askpos(0, 0, 127);
						local_var_5c = aaske(0, 0x1000034);
						mesclose(0);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						if (local_var_5c == 1)
						{
							break;
						}
					}
					gillwinstart((-1 * local_var_5e));
					gillwinsync();
					subgill(local_var_5e);
					if (!((quest_global_flag[4] & 1)))
					{
						amese(0, 0x1000035);
						local_var_5b = 1;
						quest_global_flag[4] = (quest_global_flag[4] | 1);
					}
					else
					{
						amese(0, 0x1000036);
					}
					messync(0, 0);
					local_var_5b = 1;
				}
				break;
			default:
				amese(0, 0x1000037);
				messync(0, 0);
				break;
		}
		gillwinclose();
		if (local_var_5b == 1)
		{
			suspendbattle();
			if (local_var_5c < 0)
			{
				sebsoundplay(0, 84);
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 30);
				fadesync_d3(2);
				voicestopall();
				setchocobomode(1);
				partyallread();
				partystdmotionplay_540(0x1000000, 0);
				setmapjumpgroupflag(115);
			}
			else
			{
				sebsoundplay(0, 84);
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 30);
				fadesync_d3(2);
				voicestopall();
				sysReq(1, choco_sum_ctrl::summon_off_tag);
				sysReqwait(choco_sum_ctrl::summon_off_tag);
				setchocobomode(1);
				partyallread();
				partystdmotionplay_540(0x1000000, 0);
				setmapjumpgroupflag(115);
			}
			wait(10);
			resumebattle();
			mapjump(local_var_59, 1, 1);
			return;
		}
		wait(1);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function talkhold_gardy()
	{
		return;
	}


	function talkterm_gardy()
	{
		sysLookata(-1);
		motiontrigger();
		wait(1);
		motionsync();
		motionplay_bb(0x10000005, 5);
		motionsync();
		lookatoff();
		turn_62(getdestrotangzx_227(0));
		return;
	}


	function talk_crystal()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		setkubifuristatus(0);
		setkutipakustatus(1);
		setunazukistatus(1);
		amese(0, 0x1000038);
		messync(0, 1);
		setkutipakustatus(0);
		setunazukistatus(0);
		lookatoff();
		setkubifuristatus(1);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script 兵士Ａ(6)
{

	function init()
	{
		return;
	}


	function sb05_set()
	{
		hidecomplete();
		setpos(126.66053, -10, 170.22496);
		dir(2.77605104);
		bindp2(0x300000d);
		set_ignore_hitgroup(1);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		setnpcname(183);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function sb05_act()
	{
		while (true)
		{
			motionplay(0x10000006);
			wait(1);
			motionsync_282(1);
			wait(10);
		}
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(1);
		sysAturna(-1);
		stdmotionplay(0x1000011);
		setkutipakustatus(1);
		setunazukistatus(1);
		amese(0, 0x1000039);
		messync(0, 1);
		setkutipakustatus(0);
		setunazukistatus(0);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}
}


script 兵士Ｂ(6)
{

	function init()
	{
		return;
	}


	function sb05_set()
	{
		hidecomplete();
		setpos(118.513062, -10, 165.555725);
		dir(-2.02774811);
		bindp2_d4(0x300000d, 1, 2);
		set_ignore_hitgroup(1);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		setnpcname(183);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function sb05_act()
	{
		while (true)
		{
			motionplay(0x10000006);
			wait(1);
			motionsync_282(1);
			wait(15);
		}
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(1);
		sysAturna(-1);
		stdmotionplay(0x1000011);
		setkutipakustatus(1);
		setunazukistatus(1);
		amese(0, 0x100003a);
		messync(0, 1);
		setkutipakustatus(0);
		setunazukistatus(0);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}
}


script チョコボ(6)
{

	function init()
	{
		return;
	}


	function sb05_set()
	{
		hidecomplete();
		setpos(126.934311, -10, 171.485748);
		dir(-1.26060903);
		bindp2_d4(0x300000a, 1, 0);
		set_ignore_hitgroup(1);
		stdmotionread(1);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function sb05_act()
	{
		while (true)
		{
			lookat_24f(121.80291, -8, 166.677002);
			wait(45);
			lookat_24f(121.649864, -8, 180.598953);
			wait(45);
		}
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
char    file_var_27[10];         // pos: 0x44;


script モグシー(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_60;            // pos: 0x1;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		file_var_27[0] = 0;
		file_var_27[1] = 1;
		file_var_27[2] = 2;
		file_var_27[3] = -1;
		file_var_27[4] = 3;
		local_var_60 = 1;
		motionplay_bb(0x10000007, 5);
		g_iw_mogxi_use = 0;
		if (g_iw_mogxi_first_use == 0)
		{
			switch (local_var_60)
			{
				case 2:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100003b);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 1:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100003c);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 0:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100003d);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				default:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100003e);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
			}
			g_iw_mogxi_first_use = 1;
		}
		else
		{
			switch (local_var_60)
			{
				case 2:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100003f);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 1:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000040);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 0:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000041);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				default:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000042);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
			}
		}
		for (regI0 = 0; regI0 <= 4; regI0 = (regI0 + 1))
		{
			if (file_var_27[regI0] < 0)
			{
				setaskselectignore(0, regI0);
			}
		}
		askpos(0, 0, 99);
		scratch2_var_2e = aask(0, 0x1000043, 120, 0x3fe, 1);
		mesclose(0);
		messync(0, 1);
		if (scratch2_var_2e >= 0)
		{
			g_iw_mogxi_use = 1;
			g_iw_カトリーヌランダム出現場所フラグ = scratch2_var_2f;
			ucoff();
			lookatoff();
			switch (scratch2_var_2e)
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					sysReq(1, mogxiMapJumpDirector::mogjump);
					return;
				default:
					g_iw_mogxi_use = 0;
					break;
			}
		}
		else
		{
			motionplay_bb(0x10000008, 5);
		}
		g_iw_mogxi_use = 0;
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		wait(1);
		motionsync_282(1);
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_5f;            // pos: 0x0;

	function MogxiSet()
	{
		if (g_iw_mogxi_use != 0)
		{
			local_var_5f = 1;
		}
		else
		{
			local_var_5f = 0;
		}
		if (g_iw_mogxi_rab_ok == 1)
		{
			setpos(118.506096, -10, 156.745544);
			dir(1.51818097);
			bindp2_d4(0x300000f, 1, 0);
			stdmotionread(16);
			stdmotionreadsync();
			stdmotionplay(0x1000000);
			setweight(-1);
		}
		setnpcname(0x2c2);
		set_ignore_hitgroup(1);
		fetchambient();
		sysReq(1, hitactor01_mog::setHitObj);
		sysReqi(1, 停車場::MogxiSet);
		return;
	}


	function MogxiGrad()
	{
		if (local_var_5f == 1)
		{
			wait(60);
			motionplay_bb(0x12000000, 20);
		}
		return;
	}


	function MogxiHide()
	{
		hidecomplete();
		return;
	}


	function MogxiClearHide()
	{
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}
}


script 停車場(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function MogxiSet()
	{
		bindp2_d4(0x3000009, 1, 0);
		set_ignore_hitgroup(1);
		setpos(117.870926, -10, 157.877029);
		dir(1.46843696);
		setweight(-1);
		setradius_221(0.449999988, 0.449999988);
		fetchambient();
		return;
	}


	function MogxiHide()
	{
		hidecomplete();
		return;
	}


	function MogxiClearHide()
	{
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}
}


script 乗っ取り１(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_61;            // pos: 0x0;

	function init()
	{
		local_var_61 = getduplicateid();
		return;
	}


	function main(1)
	{
		return;
	}


	function talk(2)
	{
		return;
	}


	function 乗っ取る()
	{
		capturepc(-1);
		return;
	}


	function 乗っ取り解除()
	{
		releasepc();
		return 0;
	}


	function capture_and_alphafade()
	{
		return;
	}
}


script 放浪ヴィエラ(6)
{

	function init()
	{
		return;
	}


	function vie_set()
	{
		hidecomplete();
		setpos(128.943314, -10, 171.706573);
		dir(2.66156197);
		bindp2_d4(0x3000010, 4, 2);
		set_ignore_hitgroup(1);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setnpcname(157);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		setweight(-1);
		return;
	}


	function vie_action()
	{
		setunazukistatus(1);
		lookat_24f(130.237, -5, 170.343521);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		setkutipakustatus(1);
		setunazukistatus(1);
		amese(0, 0x1000044);
		messync(0, 1);
		setkutipakustatus(0);
		setunazukistatus(0);
		g_iw_ヴィエラ進行 = 1;
		fadeout(15);
		fadesync();
		g_com_navi_footcalc[0] = getx_12b(-1);
		g_com_navi_footcalc[1] = gety_12c(-1);
		g_com_navi_footcalc[2] = getz_12d(-1);
		setnavimapfootmarkstatus(0);
		sethpmenufast(0);
		settrapshowstatus(0);
		setcharseplayall(0);
		if (!(istownmap()))
		{
			setstatuserrordispdenystatus(1);
		}
		unkCall_5ac(1, 2.35800004, 100);
		resetbehindcamera(-2.79443789, 0.699999988);
		setnoupdatebehindcamera(1);
		partyusemapid(1);
		setposparty(128.780716, -10, 171.373764, -2.9732039);
		setpos(128.850006, -10, 168.389969);
		dir(-2.52214599);
		wait(3);
		setwalkspeed(getdefaultwalkspeed());
		stdmotionplay(0x1000000);
		ramove(123.168175, -10, 155.558594);
		sethpmenufast(0);
		settrapshowstatus(0);
		setcharseplayall(1);
		fadein(15);
		wait(60);
		fadeout(15);
		fadesync();
		setcharseplayall(0);
		sethpmenufast(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		setstatuserrordispdenystatus(0);
		unkCall_5ac(0, 0, 0);
		bindoff();
		setnoupdatebehindcamera(0);
		wait(3);
		showparty();
		if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
		{
			clearnavimapfootmark();
		}
		setnavimapfootmarkstatus(1);
		setcharseplayall(1);
		sethpmenufast(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		wait(1);
		fadein(15);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script モグシーズ０１(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_62;            // pos: 0x0;

	function init()
	{
		local_var_62 = getduplicateid();
		return;
	}


	function main(1)
	{
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		setkutipakustatus(1);
		setunazukistatus(1);
		amese(0, 0x1000045);
		messync(0, 1);
		setkutipakustatus(0);
		setunazukistatus(0);
		switch (local_var_62)
		{
			case 0:
				camerastart_7e(0, 0x2000000);
				break;
			default:
				camerastart_7e(0, 0x2000001);
				break;
		}
		setkutipakustatus(1);
		setunazukistatus(1);
		amese(0, 0x1000046);
		messync(0, 1);
		setkutipakustatus(0);
		setunazukistatus(0);
		cameraclear();
		setcharclipall(1);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}


	function MogxiesSet()
	{
		hidecomplete();
		switch (local_var_62)
		{
			case 0:
				if (g_iw_mogxi_rab_ok == 1)
				{
					setpos(134.301285, -10, 149.246964);
					dir(-1.12505603);
					bindp2_d4(0x3000011, 1, 1);
					stdmotionread(16);
					stdmotionreadsync();
					stdmotionplay(0x1000000);
					setweight(-1);
				}
				setnpcname(0x2cc);
				break;
			default:
				if (g_iw_mogxi_rab_ok == 1)
				{
					setpos(120.415398, -10, 135.664047);
					dir(0.823583007);
					bindp2_d4(0x3000011, 1, 1);
					stdmotionread(16);
					stdmotionreadsync();
					stdmotionplay(0x1000000);
					setweight(-1);
				}
				setnpcname(0x2cc);
				break;
		}
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		reqenable(2);
		return;
	}


	function MogxiesAct()
	{
		setwalkspeed(getdefaultrunspeed());
		while (true)
		{
			motionplay(0x11000000);
			wait(1);
			wait(1);
			motionsync_282(1);
			motionplay(0x11000001);
			wait(1);
			wait(1);
			motionsync_282(1);
			motionplay(0x11000002);
			wait(1);
			wait(1);
			motionsync_282(1);
		}
	}


	function MogxiesHide()
	{
		hidecomplete();
		return;
	}


	function MogxiesHideClear()
	{
		clearhidecomplete();
		return;
	}
}


script hitactor01(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(114.211784, -10, 162.188797);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.600000024, 0.600000024);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(114.211784, -10, 162.188797);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.600000024, 0.600000024);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor02(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(113.133911, -10, 163.185562);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(113.133911, -10, 163.185562);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor03(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(113.165314, -10, 169.822754);
		dir(-1.19237101);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1.5999999);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(113.165314, -10, 169.822754);
		dir(-1.19237101);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1.5999999);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor04(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(135.074463, -10, 162.031616);
		dir(-0.0394559987);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(2.5999999, 1);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(135.074463, -10, 162.031616);
		dir(-0.0394559987);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(2.5999999, 1);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor05(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(131.258545, -10, 169.170517);
		dir(1.93348098);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.600000024, 1);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(131.258545, -10, 169.170517);
		dir(1.93348098);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.600000024, 1);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor06(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(131.287781, -10, 164.620117);
		dir(1.51624203);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(131.287781, -10, 164.620117);
		dir(1.51624203);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor01_mog(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(118.230042, -10, 157.149826);
		dir(-1.69586301);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.300000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(118.230042, -10, 157.149826);
		dir(-1.69586301);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.300000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script map_ダミーＰＣ(6)
{

	function init()
	{
		return;
	}


	function 扉オープン()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(1);
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープン１()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(1);
		stdmotioneventsync();
		wait(8);
		return;
	}


	function 扉オープン２()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function アイテム１()
	{
		capturepc(-1);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(0);
		motionsync_282(1);
		stdmotionplay_2c2(0x100001b, 6);
		stdmotioneventsync();
		return;
	}


	function アイテム２()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック１()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック２()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック1()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック2()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ターン()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープンxyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		setforcerelax(1);
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープン１xyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		setforcerelax(1);
		stdmotioneventsync();
		wait(8);
		return;
	}


	function 扉オープン２xyz()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function ギミックアタックxyz()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック１xyz()
	{
		capturepc(-1);
		mp_map_set_angle = getangle(mp_map_set_x, mp_map_set_z);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック２xyz()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタックxyz1()
	{
		capturepc(-1);
		mp_map_set_angle = getangle(mp_map_set_x, mp_map_set_z);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタックxyz2()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function アイテム１xyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		stdmotionplay_2c2(0x100001b, 6);
		stdmotioneventsync();
		return;
	}


	function アイテム２xyz()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function ターンxyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		lookatoff();
		releasepc();
		return;
	}


	function map_behind_return()
	{
		capturepc(-1);
		setnoupdatebehindcamera(0);
		releasepc();
		return;
	}


	function ＳＥＴＰＯＳxyz()
	{
		capturepc(-1);
		setpos(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		releasepc();
		return;
	}


	function ＳＥＴＴＵＲＮxyz()
	{
		capturepc(-1);
		turnt(mp_map_set_x, mp_map_set_y, mp_map_set_z, 0);
		releasepc();
		return;
	}
}

#include "include/weather_script.c"

script PC00(7) : 0x80
{

	function init()
	{
		setupbattle(0);
		return;
	}
}


script PC01(7) : 0x81
{

	function init()
	{
		setupbattle(1);
		return;
	}
}


script PC02(7) : 0x82
{

	function init()
	{
		setupbattle(2);
		return;
	}
}


script PC03(7) : 0x83
{

	function init()
	{
		setupbattle(3);
		return;
	}
}

//======================================================================
//                           Map exit arrays                            
//======================================================================

mapExitArray mapExitGroup0[2] = {

	exitStruct mapExit0 = {
		119.953667, -10, 127, 1, 
		128.046341, -10, 127, 0x140f
	};

	exitStruct mapExit1 = {
		138.239944, -10, 140.531555, 1, 
		138.239944, -10, 147.529907, 0x150f
	};

};

mapExitArray mapExitGroup1[1] = {

	exitStruct mapExit0 = {
		112.119179, -10, 198, 1, 
		135.87674, -10, 198, 0x10f
	};

};

mapExitArray mapExitGroup2[1] = {

	exitStruct mapExit0 = {
		114.999748, -10, 151.00058, 1.57079589, 
		0, 1.40129846e-45, 0, 0x0
	};

};

mapExitArray mapExitGroup3[0] = {

};

mapExitArray mapExitGroup4[0] = {

};


//======================================================================
//                      Map jump position vectors                       
//======================================================================
mjPosArr1 mapJumpPositions1[7] = {

	mjPos mapJumpPos0 = {
		114.990082, -10, 145.99202, 3.14159226,
		0, 0, 0, 0
	};

	mjPos mapJumpPos1 = {
		124, 0, 132, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos2 = {
		133, 0, 144, -1.57079613,
		0, 0, 0, 0
	};

	mjPos mapJumpPos3 = {
		124, 0, 194, 3.14159179,
		0, 0, 0, 0
	};

	mjPos mapJumpPos4 = {
		114.990082, -10, 145.99202, 3.14159226,
		0, 0, 0, 0
	};

	mjPos mapJumpPos5 = {
		114.990082, -10, 145.99202, 3.14159226,
		0, 0, 0, 0
	};

	mjPos mapJumpPos6 = {
		114.990082, -10, 145.99202, 3.14159226,
		0, 0, 0, 0
	};

};
mjPosArr2 mapJumpPositions2[5] = {

	mjPos mapJumpPos0 = {
		114.990082, -10, 145.99202, 3.14159226,
		0, 0, 0, 0
	};

	mjPos mapJumpPos1 = {
		124, 0, 132, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos2 = {
		133, 0, 144, -1.57079613,
		0, 0, 0, 0
	};

	mjPos mapJumpPos3 = {
		124, -10, 365.025299, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos4 = {
		124, 0, 194, 3.14159179,
		0, 0, 0, 0
	};

};


//======================================================================
//                       Unknown position arrays                        
//======================================================================
unkPos1 unknownPosition0[7] = {124, -10, 127, 5.3445282, 3.88450074, 0, 0};
unkPos1 unknownPosition1[7] = {124, -10, 127, 1.94437158, 0.649999976, 0, 0};
unkPos1 unknownPosition2[7] = {137.816833, -10, 144, 3.80276394, 4.13000011, 0, 0};
unkPos1 unknownPosition3[7] = {137.816833, -10, 144, 1, 0.649999976, 1.57079613, 0};

unkPos2 unknownPosition0[7] = {124, -8.80000019, 127, 1.84137499, 1.84137499, 0, 0};
unkPos2 unknownPosition1[7] = {137.816833, -8.80000019, 144, 1.79999995, 1.79999995, 0, 0};


//======================================================================
//                          Unknown u16 Arrays                          
//======================================================================
unk16Arr1 unknown16Arrays1[2] = {
	unk16ArrEntry unknown16Array0 = {0, 1, 0, 0, 1, 0, 0, 0};
	unk16ArrEntry unknown16Array1 = {2, 3, 1, 0, 1, 1, 0, 0};
};
unk16Arr2 unknown16Arrays2[22] = {
	unk16ArrEntry unknown16Array0 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array1 = {0, 0, 0, 0, 1, 14, 28, 0};
	unk16ArrEntry unknown16Array2 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array3 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array4 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array5 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array6 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array7 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array8 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array9 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array10 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array11 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array12 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array13 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array14 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array15 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array16 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array17 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array18 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array19 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array20 = {0, 1, 16486, 16486, 1, 65535, 0, 0};
	unk16ArrEntry unknown16Array21 = {0, 1, 0, 0, 1, 65535, 0, 0};
};
unk16Arr3 unknown16Arrays3[6] = {
	unk16ArrEntry unknown16Array0 = {4, 768, 1, 1};
	unk16ArrEntry unknown16Array1 = {5, 768, 1, 1};
	unk16ArrEntry unknown16Array2 = {6, 768, 1, 1};
	unk16ArrEntry unknown16Array3 = {7, 768, 1, 1};
	unk16ArrEntry unknown16Array4 = {8, 768, 1, 1};
	unk16ArrEntry unknown16Array5 = {9, 768, 1, 0};
};

//======================================================================
//                       REQALL/REQWAITALL arrays                       
//======================================================================
reqa   reqArr0[] = {
	テレポ監督::checkTelepoCondition
};

reqa   reqArr1[] = {
	テレポ監督::getTelepoFinished
};

reqa   reqArr2[] = {
	モグシーズ０２::MogxiesSet,
	モグシーズ０１::MogxiesSet
};

reqa   reqArr3[] = {
	モグシーズ０２::MogxiesAct,
	モグシーズ０１::MogxiesAct
};

reqa   reqArr4[] = {
	モグシーズ０２::MogxiesHide,
	モグシーズ０１::MogxiesHide
};

reqa   reqArr5[] = {
};

reqa   reqArr6[] = {
	モグシー::MogxiHide,
	停車場::MogxiHide
};

reqa   reqArr7[] = {
	モグシー::MogxiClearHide,
	停車場::MogxiClearHide
};

reqa   reqArr8[] = {
};

reqa   reqArr9[] = {
	モグシーズ０２::MogxiesHideClear,
	モグシーズ０１::MogxiesHideClear
};

reqa  reqArr10[] = {
	NPC02::NPC配置,
	NPC03::NPC配置,
	NPC04::NPC配置,
	NPC05::NPC配置,
	NPC06::NPC配置,
	NPC07::NPC配置,
	NPC08::NPC配置,
	NPC09::NPC配置,
	NPC10::NPC配置,
	NPC11::NPC配置,
	NPC12::NPC配置,
	NPC13::NPC配置,
	NPC14::NPC配置,
	NPC15::NPC配置,
	NPC16::NPC配置,
	NPC17::NPC配置,
	NPC01::NPC配置
};

reqa  reqArr11[] = {
	兵士Ａ::sb05_set,
	兵士Ｂ::sb05_set,
	チョコボ::sb05_set
};

reqa  reqArr12[] = {
	兵士Ａ::sb05_act,
	兵士Ｂ::sb05_act,
	チョコボ::sb05_act
};


